"""
Created on 6/17/2022
Created by Grace Li

This script generates a figure consisting of a row of line plots of comparing the results of the simulations for 
our DW model with node weights on complete graphs with different sizes (number of nodes N).

In the paper, we examine the numbers of major and minor clusters, Shannon entropy, mean local receptiveness and convergence time.
Here, we also plot the variance of final opinions and the convergence time in terms of time-steps where opinions changed.

For each quantity of interest, this script generates a figure consisting of a grid of line plots. The rows of the grid correspond to the 
compromise parameter m (called mu in the code), and the columns in the figure correspond to the confidence bound c (called d in the code).
For each line plot, we show the quantity of interest versus the number of nodes N (the graph size). There are curves on each line plot
that correspond to the different node-weight distributions (indicated by color and marker) and error bars representing one standard deviation.

We generate each point in each line plot from 100 numerical simulations from 10 sets of node weights that each have 10 sets of initial opinions.

"""

"""
Note 5/2/2024: Initially we had called the distributions that had the same mean as a Pareto distribution with parameter alpha = log(0.1) / log(0.2/0.9) "80-10" distributions. However, as we detail in our erratum such distributions are actually "80-43". In this script, we have changed the names to "Pareto-80-43", "Exp-80-43", and "Uniform-80-43" so that the distribution name now correct match.
"""

import numpy as np
import pandas as pd
from scipy import io, stats
import os
import random
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import animation
import seaborn as sns
import time as time
import igraph as igraph

# Import our own DW module
import sys
sys.path.append('..') #look one directory above
import DW as DW

# sns.set_style("ticks",{'axes.grid' : True})

## Change parameters here -------------------------------------

# Define graph type and weight distribution
graph_type = "complete"
distributions = ["Constant", "Uniform-80-43", "Exp-80-43", "Pareto-80-43"]

#Specify which quantities to generate plots of
keys = [
        'log(T)', 'log(T_changed)', 
        'entropy',
        'n_minor', 'n_clusters_major',
        'avg_local_receptiveness',
        'op_var'
       ]
# keys = ['log(T)']

#Specify folder name for saving plots
directory = 'GraphSizePlots/combined'
#Check that a directory exists, and if not, create it
if not os.path.exists(directory):
    os.makedirs(directory)
    
#specify graph sizes (number of nodes)
# Ns = [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000]
Ns = [10, 20, 30, 45, 65, 100, 150, 200, 300, 400, 500, 600, 700, 800, 900, 1000]

#specify confidence bound ds and compromise parameters mus

'''
NOTE: THE CONFIDENCE BOUND IS CALLED d HERE, BUT c IN THE PAPER
NOTE: THE COMPROMISE PARAMETER IS CALLED mu HERE, BUT m IN THE PAPER
'''

ds = [0.1, 0.3, 0.5]
mus = [0.3, 0.5]


## Plot parameters ----------------------------------------------
# sns.set_theme(style="whitegrid")

#Plot font size parameters
fontsizes = {'XS': 10, 'S': 22, 'M': 28, 'L':38, 'XL': 50} 

plot_height = 5.5
plot_width = 6.5

rows, cols = len(mus), len(ds)
figsize = (plot_width*cols, plot_height*rows)

bbox_to_anchor=(1.0, 0.5)

#Names for plots and save files
plot_name = {
                # 'log(T)': 'T', 
                'log(T)': 'Tf', 
                'log(T_changed)': 'T_changed',
                'n_clusters': 'clusters', 'entropy' : 'entropy',
                'n_minor' : 'minor_clusters',
                'n_clusters_major' : 'major_clusters',
                'entropy_major' : 'entropy_major',
                'avg_opinion_diff': 'op_diff', 
                'avg_local_agreement' : 'agreement', 
                'avg_local_receptiveness': 'receptiveness',
                'op_mean': 'op_mean', 'op_var': 'op_var', 
                'op_skew': 'op_skew', 'op_kurtosis': 'op_kurtosis'
            }
plot_title = {
                # 'log(T)': r"$\log_{10}(T)$",
                'log(T)': r"$\log_{10}(T_f)$",
                'log(T_changed)': r"$\log_{10}(T_\mathrm{changed})$",
                'n_clusters': 'Number of clusters', 
                'entropy': 'Shannon entropy',
                'n_minor' : 'Number of minor clusters',
                'n_clusters_major' : 'Number of major clusters',
                'entropy_major' : 'Shannon entropy of major clusters',
                'avg_opinion_diff': 'Mean of opinion difference',
                'avg_local_agreement': 'Mean of local agreement', 
                'avg_local_receptiveness': 'Mean of local receptiveness', 
                'op_mean': 'Opinion mean', 'op_var': 'Opinion variance', 
                'op_skew': 'Opinion skew', 'op_kurtosis': 'Opinion kurtosis'
             }


#For each graph size, load the graph-level simulation results stored csv files and combine them

# columns = ['N', 'distribution', 'd', 'mu', 'weight_set', 'opinion_set',
#            'n_clusters', 'n_minor', 'n_clusters_major', 
#            'bailout', 'entropy', 'entropy_major', 
#            'avg_local_agreement', 'avg_local_receptiveness',
#            'T', 'T_changed', 'avg_opinion_diff',
#            'op_mean', 'op_var', 'op_skew', 'op_kurtosis']

#Here's a quick function to do so
def get_sim_results(n, distribution):
    #Read the stored simulation results for this graph size
    filename = graph_type + str(n) + "/" + distribution + '/combined_matfiles/simulation_results.csv'
    sim_results = pd.read_csv(filename)

    #Include the graph size and distribution in the data frame
    sim_results['N'] = [int(n)] * len(sim_results)
    sim_results['distribution'] = [distribution] * len(sim_results)
    
    #Make sure the graph, weight_set and opinion_set, and time steps are integers instead of floats
    sim_results = sim_results.astype({'weight_set': int, 'opinion_set': int,
                                     'T': int, 'T_changed': int, 'N': int})

    #Calculate the natural log of time steps to make it easier to visualize
    sim_results['log(T)'] = np.log10(sim_results['T'])
    sim_results['log(T_changed)'] = np.log10(sim_results['T_changed'])
    
    return sim_results


## Generate the plots for each quantity of interest
for key in keys:
    print(key)

    fig, axs = plt.subplots(rows, cols, constrained_layout=True, figsize=figsize)
                            # sharex=True) #, sharey=True)
    
    summary_df = pd.DataFrame()
    
    #Generate each subplot
    for row in range(rows):
        for col in range(cols):
            
            #Get the d and mu values and the axis for this plot
            ax = axs[row, col]
            d = ds[col]
            mu = mus[row]
            
            #Initialize the dataframe to get all the results for this d, mu combo
            sim_results = pd.DataFrame()

            #For each graph size n and weight distribution, get the simulation results
            for n in Ns:

                for distribution in distributions:
                    #Get the combined dataframe of simulation outputs for this graph size n
                    df = get_sim_results(n, distribution)

                    #filter the data table by the d and mu values
                    df = df[df['d'] == d]
                    df = df[df['mu'] == mu]

                    sim_results = pd.concat([sim_results, df], ignore_index=True)
                    del df

                    #Get the summary statistics for each simulation point to save to csv file
                    temp_series = sim_results[sim_results['N'] == n][key].astype('float')
                    entry = pd.DataFrame({'key': key, 'c': d, 'm': mu, 'N': n, 
                                        'distribution': distribution,
                                        'mean': temp_series.mean(),
                                        'std': temp_series.std(),
                                        'min': temp_series.min(),
                                        'max': temp_series.max()
                                       }, index = [0])
                    summary_df = pd.concat([summary_df, entry], ignore_index=True)
                    
            sim_results = sim_results.astype({'N': int}) #make sure the graph size is showing up as an integer
            sim_results["log10_N"] = np.log10(sim_results['N'])
            
            #Generate pointplot for visualization for this d, mu combo
            sns.lineplot(x="N", y=key, ax = ax, data=sim_results, hue='distribution', style = 'distribution',
                          errorbar="sd", err_style="bars", linewidth = 3,
                          palette = ['#0173b2', '#cc78bc', '#029e73', '#de8f05'],
                          markers = ['o', 'd', '^', 's'], dashes=False, markersize=14)
            
            title = r"$c = $" + str(d) + r", $m = $" + str(mu)
            ax.set_title(title, fontsize = fontsizes['L'], pad=fontsizes['XS'])
            
            ax.set(ylabel=None)
            ax.set(xlabel=None)
            
            ax.set_xscale('log')
            ax.tick_params('both', length=10, width=1, which='minor')
            ax.tick_params('both', length=10, width=2, which='major')
            ax.xaxis.set_major_formatter(mpl.ticker.ScalarFormatter())
            ax.tick_params(axis='both', labelsize = fontsizes['S'])
            # ax.tick_params(axis = 'x', rotation = 45)
            
            #Hide the legend
            ax.get_legend().remove()

            #For the first gamma plot we generate, create a global legend in the last corner
            if row == 0 and col == 0:
                fig.legend(title = "Node-weight\n" + "distribution", loc='center left', bbox_to_anchor=bbox_to_anchor,
                          fontsize = fontsizes['M'], title_fontsize = fontsizes['L'], markerscale=2)
                # ax.legend(markerscale=6)
     
    summary_df.to_csv(f"GraphSizePlots/tables/{plot_name[key]}.csv")
    
    suptitle = plot_title[key] + ' for complete graphs'
    # fig.suptitle(suptitle, fontsize = fontsizes['XL'])
    
    fig.supxlabel(" "*3 + r"Graph size ($N$)", fontsize = fontsizes['XL'])
    fig.supylabel(" "*2 + plot_title[key], fontsize = fontsizes['XL'])

    #Save the plot
    savefile = directory + f'/graph_size--{plot_name[key]}.png'
    plt.savefig(savefile, bbox_inches='tight', facecolor='white')
    plt.show()