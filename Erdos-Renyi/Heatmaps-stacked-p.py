"""
Created on 3/21/2022
Created by Grace Li

This script generates a figure consisting of a grid of heatmaps of the results of the simulations for our DW model with node weights 
on Erdos-Renyi random graphs. Each row of heatmaps represents a edge-probability p, and each heatmap represents the results for a 
single node-weight distribution for that p value.

In the paper, we examine the numbers of major and minor clusters, Shannon entropy, mean local receptiveness and convergence time.
Here, we also plot the variance of final opinions and the convergence time in terms of time-steps where opinions changed.

For each quantity of interest, this script generates a figure consisting of a row of heatmaps. Each heatmap represents a single 
distribution of node weights, and has the confidence bound c (named d in the code) on the vertical axis and the compromise 
parameter m (named mu in the code) on the horizontal axis. There is an option to show labels on each cell of the heatmaps 
which can represent the mean value of our simulations +/- one standard deviation. 

We generate each cell in each heatmap from from 500 numerical simulations from 5 random graphs each with 10 sets of node weights 
that each have 10 sets of initial opinions.

"""

"""
Note 5/2/2024: Initially we had called the distributions that had the same mean as a Pareto distribution with parameter alpha = log(0.1) / log(0.2/0.9) "80-10" distributions. However, as we detail in our erratum such distributions are actually "80-43". In this script, we have changed the names to "Pareto-80-43", "Exp-80-43", and "Uniform-80-43" so that the distribution name now correct match.
"""

import numpy as np
import pandas as pd
from scipy import io
import sys
import random
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import animation
import seaborn as sns
import time as time
import igraph as igraph
import os.path
from os import getpid
import multiprocessing

# Import our own DW module
import sys
sys.path.append('..') #look one directory above
import DW as DW

## Change parameters here ------------------------------------------------

#name of experiment folder
graph_type = "erdos-renyi"
n = 500
ps = [0.7, 0.5, 0.3, 0.1] #Erdos-renyi edge probability

#specify the numbers of the random graphs
graphs = list(range(5))

show_labels = True

print_std = True
decimal_places = 2
if not show_labels:
    print_std = False

# distributions = ["Constant", "Pareto-80-43", "Pareto-80-20"]#, "Pareto-90-10"]
# distribution_name = "Pareto"

# distributions = ["Constant", "Exp-80-43", "Exp-80-20"]#, "Exp-90-10"]
# distribution_name = "Exponential"

# distributions = ["Constant", "Uniform-80-43", "Uniform-80-20"]#, "Uniform-90-10"]
# distribution_name = "Uniform"

# distributions = ["Constant", "Uniform-80-20", "Exp-80-20", "Pareto-80-20"]
# distribution_name = "80-20"

distributions = ['Constant',
                 'Uniform-80-43', 'Uniform-80-20',
                 'Exp-80-43', 'Exp-80-20',
                 'Pareto-80-43', 'Pareto-80-20']
distribution_name = 'All'

keys = [
        'log(T)', 'log(T_changed)', 
        'entropy',
        'n_minor', 'n_clusters_major',
        'avg_local_receptiveness',
        'op_var'
       ]
# keys = ['log(T)']

#Check that a directory exists, and if not, create it
directory = graph_type + str(n) + '/combined_plots/heatmaps/stacked-p/' + distribution_name +('/labeled' * show_labels)
if not os.path.exists(directory):
    os.makedirs(directory)
    
    
'''
NOTE: THE CONFIDENCE BOUND IS CALLED d HERE, BUT c IN THE PAPER
NOTE: THE COMPROMISE PARAMETER IS CALLED mu HERE, BUT m IN THE PAPER
'''

#compromise parameter
mus = [0.1, 0.3, 0.5]

#Define the confidence bound sections for each distribution
#The default d values and subfigure heights
all_ds = [
            [0.1, 0.2], 
            [0.25, 0.3, 0.35, 0.4],
            [0.5, 0.7, 0.9]
         ]
subfig_heights = [2, 3, 3]

#flip ds so that heatmaps are ordered properly
all_ds = [np.flip(ds) for ds in all_ds]
all_ds.reverse()
subfig_heights.reverse()


# Plot parameters ---------------------------------------------------------------------

fig_height = 8*len(ps)
if len(ps) <= 3:
    fig_height = 7*len(ps)
    
base_fontsizes = {'XXS': 5, 'XS': 15, 'S': 25, 'M': 35, 'L':40, 'XL': 40, 'XXL':80}
if distribution_name == 'All':
    base_fontsizes = {'XXS': 5, 'XS': 15, 'S': 30, 'M': 40, 'L': 45, 'XL': 50, 'XXL': 100}
if show_labels:
    base_fontsizes = {'XXS': 5, 'XS': 20, 'S': 25, 'M': 30, 'L': 35, 'XL': 45, 'XXL':80}
    if distribution_name == 'All':
        base_fontsizes = {'XXS': 5, 'XS': 20, 'S': 30, 'M': 45, 'L': 55, 'XL': 60, 'XXL': 100}

# if len(ps) >= 2:
#     base_fontsizes['XXL'] = 100
    
row_label_width = 0.5
width_ratios = [1.12] + [1] * (len(distributions) - 1)#  + [row_label_width]

colorbar_title_spaces = 0
if distribution_name == "All":
    colorbar_title_spaces = 3
    row_label_width = 0.7

border_width = 1
    
#These are the keys that require plots printed in exponential notation and a wider figure size
#figsize = (fig_width * len(distributions), sum(subfig_heights)) #width x height
base_fig_width = 5.6
base_cbar_height =0.04

if show_labels:
    base_fig_width = 7
    base_cbar_height =0.04

    wider_figure_keys = ['entropy', 'entropy_major']
    wider_fig_width = 8
    wider_cbar_height =0.05

    widest_figure_keys = ['avg_opinion_diff', 'avg_local_receptiveness', 'op_var']
    widest_fig_width = 9
    widest_cbar_height =0.06
    
# base_cmap = "inferno"
# reverse_cmpa = "inferno_r"

nodes = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.8, 0.9, 1.0]
colors = ['#ffffd9','#edf8b1','#c7e9b4','#7fcdbb','#41b6c4','#1d91c0','#225ea8','#253494','#081d58', '#002652'] #Yellow green blue
# colors = ['#fff7f3','#fde0dd','#fcc5c0','#fa9fb5','#f768a1','#dd3497','#ae017e','#7a0177','#49006a', '#0e0014'] #Red purple
base_cmap = mpl.colors.LinearSegmentedColormap.from_list("mycmap", list(zip(nodes, colors)))
colors.reverse() #flip the colors
reverse_cmap = mpl.colors.LinearSegmentedColormap.from_list("mycmap", list(zip(nodes, colors)))
colors.reverse() #flip back to the original in case we need them again

#Names for plots and save files
plot_name = {
                # 'log(T)': 'T', 
                'log(T)': 'Tf', 
                'log(T_changed)': 'T_changed',
                'n_clusters': 'clusters', 'entropy' : 'entropy',
                'n_minor' : 'minor_clusters',
                'n_clusters_major' : 'major_clusters',
                'entropy_major' : 'entropy_major',
                'avg_opinion_diff': 'op_diff', 
                'avg_local_agreement' : 'agreement', 
                'avg_local_receptiveness': 'receptiveness',
                'op_mean': 'op_mean', 'op_var': 'op_var', 
                'op_skew': 'op_skew', 'op_kurtosis': 'op_kurtosis'
            }
plot_title = {
                # 'log(T)': r"$\log_{10}(T)$",
                'log(T)': r"$\log_{10}(T_f)$",
                'log(T_changed)': r"$\log_{10}(T_\mathrm{changed})$",
                'n_clusters': 'Number of clusters', 
                'entropy': 'Shannon entropy',
                'n_minor' : 'Number of minor clusters',
                'n_clusters_major' : 'Number of major clusters',
                'entropy_major' : 'Shannon entropy of major clusters',
                'avg_opinion_diff': 'Mean of opinion difference',
                'avg_local_agreement': 'Mean of local agreement', 
                'avg_local_receptiveness': 'Mean of local receptiveness', 
                'op_mean': 'Opinion mean', 'op_var': 'Opinion variance', 
                'op_skew': 'Opinion skew', 'op_kurtosis': 'Opinion kurtosis'
             }

#Statistics to look at
stats = ['avg', 'std']
save_suffix = {'avg':'_avg', 'std':'_std'}
title_prefix = {'avg':'Mean of ', 'std': 'STD of '}
title_prefix = {'avg':'', 'std': 'STD of '}


## Generate heatmap figures ------------------------------------

#Define dataframe columns from conslidated matfile
columns = ['graph', 'd', 'mu', 'weight_set', 'opinion_set',
           'n_clusters', 'n_minor', 'n_clusters_major', 
           'bailout', 'entropy', 'entropy_major', 
           'avg_local_agreement', 'avg_local_receptiveness',
           'T', 'T_changed', 'avg_opinion_diff',
           'op_mean', 'op_var', 'op_skew', 'op_kurtosis']

#Loop through the quantities of interest
for key in keys:
    
    print(key)
    
    #Pick the colormap based on if we want higher or lower numbers more important
    if key in ['avg_local_receptiveness']: #lower numbers are more important
        cmap = reverse_cmap
    else:
        cmap = base_cmap
        
    #Pick the figure size parameters based on the key
    fig_width = base_fig_width
    fontsizes = base_fontsizes.copy()
    cbar_height = base_cbar_height
    if show_labels:
        if key in wider_figure_keys:
            fig_width = wider_fig_width
            cbar_height = wider_cbar_height
            for size_key in fontsizes.keys():
                fontsizes[size_key] = fontsizes[size_key] * 1.05
        elif key in widest_figure_keys:
            fig_width = widest_fig_width
            cbar_height = widest_cbar_height
            for size_key in fontsizes.keys():
                fontsizes[size_key] = fontsizes[size_key] * 1.05
    
    figsize = (fig_width * len(distributions), fig_height) #width x height

    #Get all the heatmap sections for plotting
    heatmaps = {} #dictionary to store the heatmaps for each distribution
    
    for p in ps:
        for distribution in distributions:

            #savefolder name for experiment
            experiment = graph_type + str(n) + "/" + distribution + '/combined_matfiles/p' + str(p)

            #Initialize the dataframe
            sim_results = pd.DataFrame(columns = columns)

            #Fill in the data frame row by row and store for each graph number
            for graph_number in graphs:

                filename = experiment + '/graph' + str(graph_number) + '_simulation_results.csv'
                df = pd.read_csv(filename)

                sim_results = pd.concat([sim_results, df], ignore_index=True)
                del df

            #Make sure the graph, weight_set and opinion_set, and time steps are integers instead of floats
            sim_results = sim_results.astype({'weight_set': int, 'opinion_set': int, 'T': int, 'T_changed': int})

            #Calculate the log of time steps to make it easier to visualize
            sim_results['log(T)'] = np.log10(sim_results['T'])
            sim_results['log(T_changed)'] = np.log10(sim_results['T_changed'])

            #For each segment of the heatmap (a sublist ds in all_ds)
            for section in range(len(all_ds)):

                ds = all_ds[section] #Get the confidence bound d values for this section

                #Create zero matricies for each heatmap we want to generate
                average, std = np.zeros((len(ds), len(mus))), np.zeros((len(ds), len(mus)))

                #Fill in the heatmap values 
                # rows correspond to d
                for row in range(0,len(ds)):
                    d = ds[row]

                    # columns correspond to mu
                    for col in range(0, len(mus)):
                        mu = mus[col]

                        #filter the data table by the d and mu values
                        df = sim_results[sim_results['d'] == d]
                        df = df[df['mu'] == mu]

                        #If we didn't look at this d and mu combo for the distribution plot nan (blank cell)
                        if len(df) == 0:
                            average[row][col] = np.nan
                            std[row][col] = np.nan

                        else:
                            #Calculate the descriptive stats
                            stats = df[key].describe()

                            #Add the stats values to the appropriate matrix (heatmap)
                            average[row][col] = stats['mean']
                            std[row][col] = stats['std']

                #store the heatmaps
                heatmap_key = 'p' + str(p) + '-' + distribution + '-' + str(section)
                heatmaps[heatmap_key] = {'avg':average, 'std':std}
 

    #Plot the heatmaps
    for stat in ['avg']: #in heatmaps[distribution].keys():
        
        plot_maps = {heatmap_key: heatmaps[heatmap_key][stat] for heatmap_key in heatmaps.keys()} #collect all the heatmaps of the same quantity - i.e. average, std
        
        #Generate the figure and grid of subfigures for each heatmap
        fig = plt.figure( facecolor='white', figsize = figsize, constrained_layout=True )
        master_subfigs = fig.subfigures(1, 2, width_ratios = [sum(width_ratios), row_label_width])
        subfigs = master_subfigs[0].subfigures( len(ps), len(distributions), width_ratios = width_ratios)
        row_label_subfigs = master_subfigs[1].subfigures(len(ps), 1)
        
        #Add a giant combined colorbar at the bottom
        #cbar_ax = fig.add_axes([1.01, .1, .02, .8]) #add colorbar - left, bottom, width, height
        master_subfig_width = 1 - row_label_width / (sum(width_ratios) + row_label_width)
        width = 0.8*master_subfig_width
        left = 0.125*master_subfig_width
        cbar_ax = fig.add_axes([left, -0.10, width, cbar_height]) 
    
        #Get the maximum value for the colorbar
        vmax = [np.nanmax(plot_maps[heatmap_key]) for heatmap_key in plot_maps.keys()]
        vmax = DW.round_decimals_up(max(vmax), 1)
    
        #Get the minimum value for the color bar
        vmins = []
        for heatmap_key in plot_maps.keys():
            matrix = plot_maps[heatmap_key]
            # If we have -inf values, i.e. if a value is ln(0), then set to 0 so it will plot
            if (np.amin(matrix) == - np.inf):
                matrix[matrix == - np.inf] = 0
                plot_maps[heatmap_key] = matrix
                vmins.append(0)
            else:
                vmins.append(DW.round_decimals_down(np.nanmin(matrix), 1))
        vmin = min(vmins)
        
        #Override vmin to 0 if we are looking at n_clusters
        if key == 'n_clusters':
            vmin = 0
    
        #Make a row of heatmaps for each p value
        for row in range(len(ps)):
            p = ps[row]
            #PLot the heatmap for each distribution
            for col in range(len(distributions)):
                
                distribution = distributions[col]
                subfig = subfigs[row, col] 

                #Section off the subfigure for this distribution into different axs for each heatmap section
                axs = subfig.subplots(len(all_ds), 1, gridspec_kw={'height_ratios': subfig_heights})
                    
                for k in range(len(all_ds)):
                    ax = axs[k] #single out the right nested subfigure

                    #Set title for the heatmap for this distribution if we're in the top section
                    if k == 0:
                        if distribution in ['Uniform-80-43', 'Uniform-80-20', 'Uniform-90-10']:
                            ax.set_title('Unif' + distribution[7:], fontsize = fontsizes['L'])
                        else:
                            ax.set_title(distribution, fontsize = fontsizes['L'])

                    # heatmap_key = distribution + '-' + str(k)
                    heatmap_key = 'p' + str(p) + '-' + distribution + '-' + str(k)
                    matrix = plot_maps[heatmap_key] #get the matrix for the heatmap
                    ds = all_ds[k] #get the list of ds for this section

                    #Convert matrix to dataframe for axis labels
                    df = pd.DataFrame(matrix, index = ds, columns = mus)

                    #Plot heat map with or without entry values depending on show_values
                    if show_labels:
                        #Get the annotated values of average +/- std
                        if print_std and stat == 'avg':
                            
                            #Get properly formatted precision on labels of avg +/- std
                            avg = np.array(df)
                            std = heatmaps[heatmap_key]['std']
                            labels = DW.print_avg_with_std(avg, std)

                            #Plot heat map with labels of average +/- std
                            g = sns.heatmap(df, ax = ax, 
                                            cbar = True, cbar_ax = cbar_ax, cmap = cmap, cbar_kws={"orientation": "horizontal"},
                                            vmin = vmin, vmax = vmax, annot = labels, fmt = "", annot_kws={"size":fontsizes['XS']})

                        else:
                            g = sns.heatmap(df, ax = ax, 
                                            cbar = True, cbar_ax = cbar_ax, cmap = cmap, cbar_kws={"orientation": "horizontal"},
                                            vmin = vmin, vmax = vmax, annot = True,
                                            fmt="0."+str(decimal_places)+"f", annot_kws={"size":fontsizes['XS']})
                    else:
                        g = sns.heatmap(df, ax = ax, 
                                        cbar = True, cbar_ax = cbar_ax, cmap = cmap, cbar_kws={"orientation": "horizontal"},
                                        vmin = vmin, vmax = vmax, annot = False)

                    #Set y ticks only be legible (i.e. black text) if we are in the leftmost column
                    if col == 0:
                        ax.set_yticklabels(ds, rotation = 0, size = fontsizes['S'])
                    #To get the subfigures to line up we use the same tick marks but make them white (invisible) otherwise
                    else:
                        # ax.set_yticklabels(ds, rotation = 0, size = fontsizes['S'])
                        # ax.tick_params(axis='y', colors='white')
                        ax.axes.yaxis.set_visible(False)

                    #Set the x ticks if we are in the bottom section of the heatmap
                    if k == len(all_ds) - 1:
                    # if (row == n_subfig_rows - 1) and (k == len(all_ds) - 1):
                        ax.set_xticklabels(mus, size = fontsizes['S'])
                    #Otherwise bootleg show the labels in white to create some space between heatmap sections
                    else:
                        ax.set_xticklabels(mus, size = fontsizes['XXS'])
                        ax.tick_params(axis='x', colors='white')
            
                    #Set border for heatmaps
                    for spine in ax.spines.values():
                        spine.set(visible=True, lw=border_width, edgecolor="black")
            
            #Label the rows
            label_subfig = row_label_subfigs[row]
            ax = label_subfig.subplots()
            ax.set_axis_off()
            row_label = r"$p$ = " + str(p)
            ax.text(0.5, 0.5, row_label, ha="center", va="center", fontsize=fontsizes['XL'])
                
        ## Set colorbar and tick label font size
        cbar_ax.tick_params(labelsize=fontsizes['M'])
        cbar_ax.set_xlabel(title_prefix[stat] + plot_title[key] + " "*colorbar_title_spaces, fontsize=fontsizes['XXL'], labelpad = fontsizes['S'])
        cbar_ax.tick_params(axis='both', labelsize=fontsizes['M'])
        cbar_ax.spines["outline"].set(visible=True, lw=border_width, edgecolor="black")
        cbar_ax.xaxis.tick_top()
        
        #Large shared axis labels
        fig.supylabel(" "*3 + r"Confidence bound ($c$)", fontsize = fontsizes['XXL'])
        fig.supxlabel(r"Compromise parameter ($m$)" + " "*colorbar_title_spaces, fontsize = fontsizes['XXL'])
    
        #Large supertitle
        # suptitle = title_prefix[stat] + plot_title[key] + " for G(n=" + str(n) + ", p)"
        # plt.suptitle(suptitle, fontsize = fontsizes['XXL'])
        
        #Save the plot
        if distribution_name != 'All':
            savefile = directory + f"/ER{n}--{distribution_name}--{plot_name[key]}{save_suffix[stat]}"
        else:
            savefile = directory + f"/ER{n}--{plot_name[key]}{save_suffix[stat]}"
        savefile = savefile + ('--labeled' * show_labels) + ('--with_std' * print_std * (stat == 'avg')) + ".png"
        plt.savefig(savefile, bbox_inches='tight', facecolor='white')
        plt.show()
        
        