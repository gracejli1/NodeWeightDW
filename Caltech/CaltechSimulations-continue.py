"""
Created on 12/8/2021
Created by Grace Li

This script continues simulations of our adaptive-confidence DW model on the Caltech network. If after running 
CaltechSimulations.py, simulations reached bailout time (but their matfiles were saved), the simulations can be continued
where they left off by entering the model parameters here. This script will read the stored matfile and continue the
simulation with a longer bailout time set. The resulting continued simulation is stored in a separate "continue" matfile.

"""

"""
Note 5/2/2024: Initially we had called the distributions that had the same mean as a Pareto distribution with parameter alpha = log(0.1) / log(0.2/0.9) "80-10" distributions. However, as we detail in our erratum such distributions are actually "80-43". In this script, we have changed the names to "Pareto-80-43", "Exp-80-43", and "Uniform-80-43" so that the distribution name now correct match.
"""

import numpy as np
import pandas as pd
from scipy import io
import sys
import random
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import animation
import seaborn as sns
import time as time
import igraph as igraph
import os.path
from os import getpid
import multiprocessing

# Make process "nicer" and lower priority
import psutil
psutil.Process().nice(3)# if on *ux

# Import our own DW module
import sys
sys.path.append('..') #look one directory above
import DW as DW

# Class for running sets of DW experiments            
class DW_experiment:
    
    #Pareto Type I Distribution scale-parameter alpha where the distribution is on [1, infty)
    pareto_a = {'Pareto-90-10': math.log(10) / math.log(9),
                'Pareto-80-20': math.log(5) / math.log(4),
                'Pareto-80-43': math.log(0.1) / math.log(0.2 / 0.9)}
    
    #Shifted exponential distribution on [1, infty) mean beta
    exp_beta = {'Exp-80-20': 6.2125,
                'Exp-90-10': 20.8543,
                'Exp-80-43': 1.8836}
                #'Exp-2' : 1} #mean is 2 for exp-2
    
    #Uniform distribution right boundary for distribution defined on [1, b]
    uniform_b = {'Uniform-80-20': 13.425,
                 'Uniform-90-10': 42.7086,
                 'Uniform-80-43': 4.7672}
                 #'Uniform-2': 3} #mean is 2 for uniform-2
    
    #Tolerance, and max timestep values
    tol = 0.02
    # Tmax = 10**8
    
    # Initialize class with graph_type and number of nodes n
    def __init__(self, G, distribution, graph_name="", Tmax = 10**8, continue_round = 0,
                 return_opinion_series = False, opinion_timestep = 1):
        '''
        Initializes class to run Deffaunt-Weisbuch simulations for a particular graph type
        
        Parameters
        ----------
        G : igraph.graph
            Fixed graph to run the model on. One example is the Caltech dataset
        graph_name : string
            String to specify the graph name when saving outputs
        distribution : string
            String specifying the distribution of node weights.
            Currently, the options are "Constant", "Pareto-80-43", "Pareto-80-20", "Pareto-90-10", 
            "Exp-80-43", "Exp-80-20", "Exp-90-10", "Uniform-80-43", "Uniform-80-20", "Uniform-90-10"
        Tmax : int, default = 10**8
            Bailout time for the simulations
        continue_round : int, default = 0
            How many time we are willing to continue the simulation from where left off.
            This is so we know were to look for past simulation files to continue from
            By default continue_round is 0 and we don't won't continue from any past simulation files
        return_opinion_series : bool, default = False
            Specify whether or not to generate and store the opinon trajectories over time
        opinion_timestep : int, default = 1
            If return_opinion_series is True, then this sets how often we get the opinion and add it to the timeseries
        '''
        
        self.G = G
        self.n = G.vcount() #get number of vertices
        
        self.graph_name = graph_name
        self.distribution = distribution
        
        self.Tmax = Tmax
        self.continue_round = continue_round
        
        self.foldername = self.distribution #savefolder name for experiment
        
        #Check that the savefolder directory exists, and if not, create it
        if not os.path.exists(self.foldername):
            os.makedirs(self.foldername)
            os.makedirs(self.foldername + '/matfiles')
            os.makedirs(self.foldername + '/txtfiles')
            os.makedirs(self.foldername + '/combined_matfiles')
            os.makedirs(self.foldername + '/plots')
        
        #Set weight distribution parameter
        if self.distribution in self.pareto_a.keys():
            self.a = self.pareto_a[self.distribution]
        elif self.distribution in self.exp_beta.keys():
            self.beta = self.exp_beta[self.distribution]
        elif self.distribution in self.uniform_b.keys():
            self.b = self.uniform_b[self.distribution]
         
        #Set up opinion series parameters and save folder if applicable
        self.return_opinion_series = return_opinion_series
        self.opinion_timestep = opinion_timestep
        if self.return_opinion_series:
            if not os.path.exists(self.foldername + '/opinion_series'):
                os.makedirs(self.foldername + '/opinion_series')
        
    def generate_seed_files(self):
        '''
        Generate and save random seed files for random weights, initial opinions and simulation repeatablity if they don't exist yet
        '''
        
        self.weight_seed_file = self.foldername + "/weight_seeds.csv"
        self.opinion_seed_file = self.foldername + "/opinion_seeds.csv"
        self.sim_seed_file = self.foldername + '/sim_seeds.csv'

        #There is only one weight seed for a fixed graph (such as Caltech)
        #so we generate and save it if it doesn't exist yet
        if not os.path.exists(self.weight_seed_file):
            df = pd.DataFrame(columns = ['weight_seed'])
            random.seed(a=None) #reset random by seeding it with the current time
            weight_seed = str(random.randrange(sys.maxsize))
            df.loc[0] = [weight_seed]
            df.to_csv(self.weight_seed_file, index=False, header=True)

        #If the opinion seed file doesn't already exist, create it
        if not os.path.exists(self.opinion_seed_file):
            df = pd.DataFrame(columns = ['weight_set', 'opinion_seed'])
            df.to_csv(self.opinion_seed_file, index=False, header=True)
        
        #If sim seed file doesn't already exist, create it
        if not os.path.exists(self.sim_seed_file):
            df = pd.DataFrame(columns = ['d', 'mu', 'weight_set', 'opinion_set', 'sim_seed'])
            df.to_csv(self.sim_seed_file, index=False, header=True)
            
        return
    
    ## Function to Run DW model for this graph and weight/opinion seeds  
    def run_DW(self, params):
    
        '''
        Runs DW experiment pn the graph G and save appropriate output files.
        
        Takes in a dictionary params, containing
        "continue_sim" - whether or not we're continuing from a past simulation
        (in this case we also need that filename "previous_matfile"),
        "d" - the confidence bound, "mu" - the compromise parameter,
        and "weight_set" and "opinion_set" - integers representing which sets in
        this overall experiment on the graph G with weight/opinion seeds
        to run the DW model on.
        '''

        ## Initial set up
        #Unpack parameters
        continue_sim = params["continue_sim"]
        if self.continue_round == 0:
            continue_sim = False
        if continue_sim:
            previous_matfile = params["previous_matfile"]
            
        d, mu = params["d"], params["mu"]
        opinion_set = params["opinion_set"]
        weight_set = params["weight_set"]
        
        #Print some parameters so we know what we're running
        print('Process Number ', getpid())
        if continue_sim:
            print('Continuing matfile', previous_matfile)
        else:
            print('Params', params)
            
            
        ## Read the random seeds if they exist, and generate and store them if they don't exist yet
        lock.acquire()
        
        #Make sure we have folders to save our outputs if we're continuing the simulations
        if continue_sim:
            directories = [self.foldername + '/txtfiles/continue' + str(self.continue_round),
                           self.foldername + '/matfiles/continue' + str(self.continue_round)]
            for directory in directories:
                if not os.path.exists(directory):
                    os.makedirs(directory)
        
        # Get the random weight set seed
        df = pd.read_csv(self.weight_seed_file)
        weight_seed = df['weight_seed'].values[0]
                
        # Get the random opinion set seed 
        df = pd.read_csv(self.opinion_seed_file)
        row = df[df['weight_set'] == weight_set]
        if len(row) == 0:
            random.seed(a=None) #reset random by seeding it with the current time
            opinion_seed = random.randrange(sys.maxsize)
            row = pd.DataFrame(columns = ['weight_set', 'opinion_seed'])
            row.loc[0] = [weight_set, str(opinion_seed)]
            df = df.append(row, ignore_index=True)
            df.to_csv(self.opinion_seed_file, index=False, header=True)
        else:
            opinion_seed = row['opinion_seed'].values[0]
            opinion_seed = int(opinion_seed)
        lock.release()
        
        ## Specify the save file names
        savename = 'd' + str(d) + '-mu' + str(mu) + '-weight' + str(weight_set)

        #Create a textfile to dump outputs
        txtfile = self.foldername + '/txtfiles/' + savename + '.txt'
        if continue_sim:
            txtfile = self.foldername + '/txtfiles/continue' + str(self.continue_round) + '/' + savename + '.txt'
        
        ## If the txtfile doesn't exist yet, create it and write the header with seed values to it
        lock.acquire()
        if not os.path.exists(txtfile):
            print(txtfile)
            with open(txtfile, 'w') as f:
                print('Experiment:', self.graph_name, file=f, flush=True)
                print('d = ', d, ' and mu = ', mu, file=f, flush = True)
                print('weight_set = ', weight_set, file=f, flush = True)
                print('weight_seed = ', weight_seed, file=f, flush = True)
                print('opinion_seed = ', opinion_seed, file=f, flush = True)
        lock.release()
            
        ## Set the node weights 
        #Check to see if the graph has any isolated nodes and set their weight to 0 so they are never drawn.
        #Get the components of the graph
        component_membership = G.clusters(mode='strong').membership
        n_components = max(component_membership)+1
        isolated_nodes = []
        for i in range(n_components):
            component = np.nonzero(np.array(component_membership)==i)[0]
            if len(component) == 1:
                isolated_nodes.append(component[0])

        ## Get the weights and opinions for weight_set and opinion_set and set nodes of G to those values
        #If a constant-weight experiment, set all weights to 1
        if self.distribution == 'Constant':
            G.vs['weight'] = [1] * self.n
        #Otherwise, for a Pareto distribution, reinitialize the random seed and generate the corresponding weight set from that seed
        elif self.distribution in self.pareto_a.keys():
            random_weight = np.random.default_rng(weight_seed)
            for i in range(weight_set + 1):
                weights = random_weight.pareto(self.a, size=self.n) + 1
            G.vs['weight'] = weights
        #Same for an exponential distribution
        elif self.distribution in self.exp_beta.keys():
            random_weight = np.random.default_rng(weight_seed)
            for i in range(weight_set + 1):
                weights = random_weight.exponential(scale=self.beta, size=self.n) + 1
            G.vs['weight'] = weights
        #Same for a uniform distribution
        elif self.distribution in self.uniform_b.keys():
            random_weight = np.random.default_rng(weight_seed)
            for i in range(weight_set + 1):
                weights = random_weight.uniform(low=1, high=self.b, size=self.n)
            G.vs['weight'] = weights
        #Set any isolated nodes to have weight 0
        if n_components > 1:
            G.vs[isolated_nodes]['weight'] = 0
            
        ## Set the initial opinions: Reinitialize the random seed and generate the corresponding opinion set from that seed
        random_opinion = np.random.default_rng(opinion_seed)
        for i in range(opinion_set + 1):
            init_opinions = random_opinion.uniform(0, 1, size=self.n)
        G.vs['opinion'] = init_opinions
        
        ## Read or create a simulation seed for DW node selection for this set of parameters (d, mu, weight_set, opinion_set)
        #Read the random seed csv file as a pandas dataframe
        lock.acquire()
        df = pd.read_csv(self.sim_seed_file)
        
        #Try to get the corresponding dataframe row for this simulation
        row = df[df['d'] == d]
        row = row[row['mu'] == mu]
        row = row[row['weight_set'] == weight_set]
        row = row[row['opinion_set'] == opinion_set]
            
        #If there isn't already an entry for this simulation generate and store a simulation seed
        if len(row) == 0:
            random.seed(a=None) #reset random by seeding it with the current time so we don't keep generating the same sim seeds
            sim_seed = random.randrange(sys.maxsize)
            row = pd.DataFrame(columns = ['d', 'mu', 'weight_set', 'opinion_set', 'sim_seed'])
            row.loc[0] = [d, mu, weight_set, opinion_set, str(sim_seed)]
            df = df.append(row, ignore_index=True)
            df.to_csv(self.sim_seed_file, index=False, header=True)
        else:
            # print('Process Number ', getpid(), 'row has a sim_seed entry') #delte line
            sim_seed = row['sim_seed'].values[0]
            if len(row) > 1:
                with open(txtfile, 'w') as f:
                    print("OH NO! Something went wrong and there are multiple sim_seeds", file=f, flush=True)
                    print(row, file=f, flush=True)
        lock.release()
            
        #Read the results of the previous simulation if we are continuing from a previous file
        if continue_sim:
            previous_results = io.loadmat(previous_matfile)
            G.vs['opinion'] = previous_results['final_opinions'][0] #Continue from the opinions where we left off
            
        #Time the DW simulation for this weight + opinion set combo
        start_time = time.time()
        
        ## Run the DW model using the simulation seed
        if continue_sim:
            outputs = DW.DW(G, d, mu, random_seed = sim_seed, t_start = previous_results['T'][0][0], Tmax = self.Tmax,
                            return_opinion_series = self.return_opinion_series, opinion_timestep = self.opinion_timestep)
        else:
            ## Run the DW model using the simulation seed
            outputs = DW.DW(G, d, mu, random_seed = sim_seed, Tmax = self.Tmax,
                            return_opinion_series = self.return_opinion_series, opinion_timestep = self.opinion_timestep)

        # Dump model outputs into file
        lock.acquire()
        with open(txtfile, 'a') as f:
            print("\n----- Weight Set %s and Opinion Set %s -----" % (weight_set, opinion_set), file=f, flush=True)

            print("t_start = %s" % previous_results['T'][0][0], file=f, flush=True)
            print("T = %s" % outputs['T'], file=f, flush=True)
            print("T_changed = %s" % outputs['T_changed'], file=f, flush=True)
            print("Number of Clusters = %s" % outputs['n_clusters'], file=f, flush=True)

            print("Cluster Membership", file=f, flush=True)
            print(outputs['clusters'], file=f, flush=True)
            
            runtime = time.time() - start_time
            print('-- Runtime was %.0f seconds = %.3f hours--' % (runtime, runtime/3600) , file=f, flush=True)
        lock.release()

        ## Define dictionary to store simulation outputs for saving to a .mat file
        save_sim = {'d': d, 'mu': mu, 'weight_set': weight_set, 'opinion_set': opinion_set, 'sim_seed': sim_seed}
      
        #Include the graph-level results
        save_sim['T'] = outputs['T']                    
        save_sim['T_changed'] = outputs['T_changed']
        save_sim['bailout'] = outputs['bailout']
        save_sim['avg_opinion_diff'] = outputs['avg_opinion_diff']

        #Include the graph-level results
        save_sim['T'] = outputs['T']                    
        save_sim['T_changed'] = outputs['T_changed']
        if continue_sim:
            save_sim['T_changed'] = outputs['T_changed'] + previous_results['T_changed'][0][0]
        save_sim['T_acc'] = outputs['T_acc']
        save_sim['bailout'] = outputs['bailout']
        save_sim['avg_opinion_diff'] = outputs['avg_opinion_diff']
        
        #Include the cluster information
        clusters = outputs['clusters']
        save_sim['n_clusters'] = outputs['n_clusters']
        for i in range(outputs['n_clusters']):
            key = 'cluster' + str(i)
            save_sim[key] = clusters[i]
            #clusters can be extracted from matfile using list = clusteri.flatten().tolist()

        #Include the node-level results as size n arrays
        save_sim['weights'] = G.vs['weight']
        save_sim['init_opinions'] = init_opinions
        save_sim['final_opinions'] = outputs['final_opinions']
        save_sim['total_change'] = outputs['total_change']
        save_sim['n_updates'] = outputs['n_updates']
        if continue_sim:
            save_sim['total_change'] = outputs['total_change'] + previous_results['total_change'][0]
            save_sim['n_updates'] = outputs['n_updates'] + previous_results['n_updates'][0][0]
        save_sim['local_agreement'] = outputs['local_agreement']
        save_sim['local_receptiveness'] = outputs['local_receptiveness']
        
        ## Save the simulation results to a matfile
        matfile = self.foldername + '/matfiles/' + savename + '-op' + str(opinion_set) +'.mat'
        if continue_sim:
            matfile = self.foldername + '/matfiles/continue' + str(self.continue_round) + '/' + savename + '-op' + str(opinion_set) +'.mat'
        io.savemat(matfile, save_sim)
    
        #If we generate the opinion trajectories, then get those too and save to an opinion_trajectories folder
        if self.return_opinion_series:
            save_sim['opinion_series'] = outputs['opinion_series']
            save_sim['opinion_timestep'] = self.opinion_timestep
            del save_sim['init_opinions']
            del save_sim['final_opinions']
            matfile = self.foldername + '/opinion_series/' + savename + '-op' + str(opinion_set) +'.mat'
            if continue_sim:
                matfile = self.foldername + '/opinion_series/continue' + str(self.continue_round) + '/' + savename + '-op' + str(opinion_set) +'.mat'
            io.savemat(matfile, save_sim)
    
def init(l):
    global lock
    lock = l
    

if __name__ == "__main__":

    ## EXPERIMENT PARAMETERS - CHANGE HERE
    Tmax = 10**9 #Bailout time for simulations
    continue_round = 1 #how many times we've tried to continue simulations (for saving files)
    
    '''
    NOTE: THE CONFIDENCE BOUND IS CALLED d HERE, BUT c IN THE PAPER
    NOTE: THE COMPROMISE PARAMETER IS CALLED mu HERE, BUT m IN THE PAPER
    '''
    
    ds = [0.1, 0.2, 0.25, 0.3, 0.35, 0.4, 0.5, 0.7, 0.9] #confidence bound
    mus = [0.1, 0.3, 0.5] #compromise parameter

    distributions = ["Constant", "Pareto-80-43", "Pareto-80-20",
                     "Exp-80-43", "Exp-80-20", "Uniform-80-43", "Uniform-80-20"]
    
    #Specify which weight and opinion sets to run
    weight_sets = list(range(0,10))
    opinion_sets = list(range(0,10))
    
    #Specify whether or not to generate the opinion series
    return_opinion_series = False
    opinion_timestep = 1
    
    #Load the Caltech dataset and get the giant connected component
    matfile = 'Caltech.mat'
    data = io.loadmat(matfile)
    G = igraph.Graph.Adjacency(data['A'].toarray(), mode = "undirected")
    print('Original number of vertices:', G.vcount())
    del data
    
    component_membership = G.clusters(mode='strong').membership
    component = np.nonzero(np.array(component_membership)==0)[0]

    G = G.induced_subgraph(component)
    print('GCC number of vertices:', G.vcount())
    
    
    ## Generate list of tuples to feed into DW_experiments as parameters
    params_list = []
    
    for distribution in distributions:
        for d in ds:
            for mu in mus:
                for weight_set in weight_sets:
                    for opinion_set in opinion_sets:

                        continue_file_found = False #Whether or not we found a matfile to continue the simulation from 
                            
                        #Filenames
                        matfile_folder = distribution + '/matfiles/'
                        filename = 'd' + str(d) + '-mu' + str(mu) + '-weight' + str(weight_set) + '-op' + str(opinion_set) + '.mat'
                        

                        #If we're trying to continue the simulation, check for the files we left off from 
                        if continue_round >= 2:
                            for i in list(range(continue_round, 0, -1)): #range(start, stop, step)
                                try: 
                                    matfile = matfile_folder + 'continue' + str(i) + '/' + filename
                                    results = io.loadmat(matfile)

                                    if i == continue_ronund:
                                        break

                                    elif results['bailout'][0][0]:
                                        param_dict = {"continue_sim": True,
                                                      "previous_matfile": matfile,
                                                      "d": d, "mu": mu, 
                                                      "weight_set": weight_set,
                                                      "opinion_set": opinion_set}
                                        params_list.append(param_dict) 
                                        continue_file_found = True
                                    break
                                except:
                                    pass

                        #If we haven't found a file to continue from yet, see if we have an original first simulation file
                        if not continue_file_found:
                            matfile = matfile_folder + filename
                            try:
                                results = io.loadmat(matfile)
                                if results['bailout'][0][0] and continue_round >= 1:
                                    param_dict = {"continue_sim": True,
                                                  "previous_matfile": matfile,
                                                  "d": d, "mu": mu, 
                                                  "weight_set": weight_set,
                                                  "opinion_set": opinion_set}
                                    params_list.append(param_dict) 
                                    continue_file_found = True
                            except:
                                param_dict = {"continue_sim": False,
                                              "d": d, "mu": mu, 
                                              "weight_set": weight_set, 
                                              "opinion_set": opinion_set}
                                params_list.append(param_dict) 

        #Initialize experiment class
        experiment = DW_experiment(G, distribution, graph_name = "Caltech", Tmax = Tmax, continue_round = continue_round,
                                   return_opinion_series = return_opinion_series, opinion_timestep = opinion_timestep)
        experiment.generate_seed_files()

        l = multiprocessing.Lock()

        with multiprocessing.Pool(processes=5, initializer=init, initargs=(l,)) as pool:
            pool.map(experiment.run_DW, params_list)