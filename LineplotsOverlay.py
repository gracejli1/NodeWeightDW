"""
Created on 11/19/2022
Created by Grace Li

This script generates a figure consisting of a row of line plots of the results of the simulations for our DW model with node weights.

In the paper, we examine the numbers of major and minor clusters, Shannon entropy, mean local receptiveness and convergence time.
Here, we also plot the variance of final opinions and the convergence time in terms of time-steps where opinions changed.

For each quantity of interest, this script generates a figure consisting of a row of line plots. Each line plot represents a single type
of group of node-weight distributions. Each line plot has the quantity of interest on the 
vertical axis and the confidence bound c (named d in the code) on the horizontal axis. Each line plot has curves to represent the
mean of the node-weight distribution (indicated by color) and the compromise parameter m (named mu in the code, indicated by marker).
There is an option to show or hide an error band representing one standard deviation.

For fixed graphs (i.e., complete graphs and the Caltech network), we generate each point on each line plot from 100 numerical
simulations from 10 sets of node weights that each have 10 sets of initial opinions. For random graphs (i.e., Erdos-Renyi and SBM graphs), 
we generate each point on each line plot from 500 numerical simulations from 5 random graphs each with 10 sets of node weights 
that each have 10 sets of initial opinions.

"""

"""
Note 5/2/2024: Initially we had called the distributions that had the same mean as a Pareto distribution with parameter alpha = log(0.1) / log(0.2/0.9) "80-10" distributions. However, as we detail in our erratum such distributions are actually "80-43". In this script, we have changed the names to "Pareto-80-43", "Exp-80-43", and "Uniform-80-43" so that the distribution name now correct match.
"""

# Import required packages
import numpy as np
import pandas as pd
from scipy import io
import sys
import random
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import animation
import seaborn as sns
import time as time
import igraph as igraph
import os.path
from os import getpid
import multiprocessing

import DW as DW #import our own DW module

sns.set_style("ticks",{'axes.grid' : True})

## Change parameters here ------------------------------------------------

#Options for graph_type are complete, erdos-renyi, SBM-2-community, SBM-core-periphery, Caltech
graph_type = "SBM-core-periphery"
n = 500 #number of nodes in synthetic graph
p = 0.5 #independent edge probability for erdos-renyi graphs

show_std = False #whether or not to include bands representing one standard deviation of our simulations

show_suptitle = False #whether to print a giant suptitle for the plot

#Specify which quantities to generate plots of
keys = [
        'log(T)', 'log(T_changed)', 
        'entropy',
        'n_minor', 'n_clusters_major',
        'avg_local_receptiveness',
        'op_var'
       ]
# keys = ['entropy']

#specify the numbers of the random graphs for erdos-renyi and SBM
graphs = list(range(5))

#Compromise parameter
'''
NOTE: THE COMPROMISE PARAMETER IS CALLED mu HERE, BUT m IN THE PAPER
'''
mus = [0.1, 0.3, 0.5]
# mus = [0.1]
single_mu = False

#Define the confidence bound sections for each distribution
'''
NOTE: THE CONFIDENCE BOUND IS CALLED d HERE, BUT c IN THE PAPER
'''     
drop_ds = [0.7, 0.9]
min_d = 0.1
max_d = 0.5

custom_xticks = True
xtick_list = [0.1, 0.2, 0.3, 0.4, 0.5]

#Distributions
distribution_groups = {'Constant':['Constant'],
                       'Uniform': ['Uniform-80-43', 'Uniform-80-20'],
                       'Exp': ['Exp-80-43', 'Exp-80-20'],
                       'Pareto': ['Pareto-80-43', 'Pareto-80-20'],
                       '80-43': ['Uniform-80-43', 'Exp-80-43', 'Pareto-80-43'],
                       '80-20': ['Uniform-80-20', 'Exp-80-20', 'Pareto-80-20'],
                       '90-10': ['Uniform-90-10', 'Exp-90-10', 'Pareto-90-10'],
                      }
if graph_type == "complete" and n == 500:
    distribution_groups['Uniform'].append('Uniform-90-10')
    distribution_groups['Exp'].append('Exp-90-10')
    distribution_groups['Pareto'].append('Pareto-90-10')
    
group_names = ['Constant', 'Uniform', 'Exp', 'Pareto']
savename_suffix = 'Overlay'

# group_names = ['Constant', 'Pareto']
# savename_suffix = 'Pareto'

# group_names = ['Constant', '80-20']
# savename_suffix = '80-20'

#Name of experiment folder
folder_name_dict = {"complete": "Complete", "erdos-renyi": "Erdos-Renyi",
                    "SBM-2-community": "SBM", "SBM-core-periphery": "SBM",
                    "Caltech": "Caltech"}
folder_name = folder_name_dict[graph_type]

#Check that a directory exists, and if not, create it
directory = folder_name 
if folder_name != 'Caltech':
    directory = directory + '/' + graph_type + str(n)
directory = directory + '/combined_plots/lineplots/' + ('title/' * show_suptitle)
if graph_type == "erdos-renyi":
    directory = directory + f"/p{p}"
if not os.path.exists(directory):
    os.makedirs(directory)

## Plot parameters -----------------------------------------------------------

fontsizes = {'XXS': 10, 'XS': 20, 'S': 25, 'M': 30, 'L':35, 'XL': 40, 'XXL':40}
if single_mu:
    fontsizes['S'] = 20

plot_height = 6
plot_width = 6

#Colorlist
#https://davidmathlogic.com/colorblind/ -> tol
# colorlist = ['#117733', '#44AA99', '#88CCEE', '#882255', '#AA4499', '#CC6677']
# colorlist = ['#332288', '#117733', '#88CCEE', '#882255', '#CC6677', '#DDCC77']
constant_distribution_colorlist = ['#b2182b','#d6604d','#f4a582']
colorlist = ['#b2182b','#d6604d','#f4a582','#2166ac', '#4393c3', '#92c5de']
if single_mu:
    # colorlist = ['#b2182b', '#2166ac']
    colorlist = ['#084081', '#2b8cbe']

constant_markerlist = ["o", "X", "^"]
markerlist = ["o", "X", "^","o", "X", "^"]

constant_dashlist = ['', (3,3), (1, 1)]
dashlist = ['', (3,3), (1, 1), '', (3,3), (1, 1)]

if graph_type == "complete" and n == 500:
    colorlist = ['#b2182b','#d6604d','#f4a582','#2166ac', '#4393c3', '#92c5de', '#762a83', '#9970ab', '#c2a5cf']
    markerlist = ["o", "X", "^","o", "X", "^", "o", "X", "^"]
    dashlist = ['', (3,3), (1, 1), '', (3,3), (1, 1), '', (3,3), (1, 1)]
    if single_mu:
        markerlist = ["o", "X", "^"]
        colorlist = ['#440154', '#31688e', '#35b779']
                     
#Names for plots and save files
plot_name = {
                'log(T)': 'T', 'log(T_changed)': 'T_changed',
                'n_clusters': 'clusters', 'entropy' : 'entropy',
                'n_minor' : 'minor_clusters',
                'n_clusters_major' : 'major_clusters',
                'entropy_major' : 'entropy_major',
                'avg_opinion_diff': 'op_diff', 
                'avg_local_agreement' : 'agreement', 
                'avg_local_receptiveness': 'receptiveness',
                'op_mean': 'op_mean', 'op_var': 'op_var', 
                'op_skew': 'op_skew', 'op_kurtosis': 'op_kurtosis'
            }
plot_title = {
                'log(T)': r"$\log_{10}(T)$", 
                'log(T_changed)': r"$\log_{10}(T_\mathrm{changed})$",
                'n_clusters': 'Number of clusters', 
                'entropy': 'Shannon entropy',
                'n_minor' : 'Number of minor clusters',
                'n_clusters_major' : 'Number of major clusters',
                # 'n_minor' : 'Number of\nminor clusters',
                # 'n_clusters_major' : 'Number of\nmajor clusters',
                'entropy_major' : 'Shannon entropy of major clusters',
                'avg_opinion_diff': 'Mean opinion difference',
                'avg_local_agreement': 'Mean local agreement', 
                'avg_local_receptiveness': 'Mean local receptiveness', 
                'op_mean': 'Opinion mean', 'op_var': 'Opinion variance', 
                'op_skew': 'Opinion skew', 'op_kurtosis': 'Opinion kurtosis'
             }

#Define dataframe columns from consolidated matfiles of random graphs
columns = ['distribution', 'graph', 'd', 'mu', 'weight_set', 'opinion_set',
           'n_clusters', 'n_minor', 'n_clusters_major', 
           'bailout', 'entropy', 'entropy_major', 
           'avg_local_agreement', 'avg_local_receptiveness',
           'T', 'T_changed', 'avg_opinion_diff',
           'op_mean', 'op_var', 'op_skew', 'op_kurtosis']



# Plot heat maps of all simulation values ----------------------------------------------------------

#Loop through the quantities of interest
for key in keys:
    print(key)
    
    fig, axs = plt.subplots(1, len(group_names), constrained_layout=True, sharey=True,
                            figsize = (plot_width*len(group_names), plot_height))
    
    for i in range(len(group_names)):
        ax = axs[i]
        distribution_subset = distribution_groups[group_names[i]]
    
        #Initialize the dataframe
        sim_results = pd.DataFrame(columns = columns)
        
        for distribution in distribution_subset:
            # print(distribution)
            
            #For random graph models, read and combine the files for each random graph
            if folder_name in ['Erdos-Renyi', 'SBM']:

                experiment = folder_name + '/' + graph_type + str(n) + "/" + distribution + '/combined_matfiles'
                if folder_name == 'Erdos-Renyi':
                    experiment = experiment + '/p' + str(p)

                #Initialize the dataframe for this distribution
                distribution_df = pd.DataFrame(columns = columns[1:])

                #Fill in the data frame row by row and store for each graph number
                for graph_number in graphs:

                    filename = experiment + '/graph' + str(graph_number) + '_simulation_results.csv'
                    df = pd.read_csv(filename)

                    distribution_df = pd.concat([distribution_df, df], ignore_index=True)
                    del df
                    
            #For non random graph models or datasets, we only have one graph
            else:

                experiment = folder_name + '/' + distribution
                if folder_name == "Complete":
                    experiment = folder_name + '/' + graph_type + str(n) + "/" + distribution 

                #Initialize the dataframe
                filename = experiment + '/combined_matfiles/simulation_results.csv'
                distribution_df = pd.read_csv(filename)
                
            distribution_df = distribution_df[distribution_df['mu'].isin(mus)]
                
            distribution_name = distribution
            if distribution in ['Uniform-80-43', 'Uniform-80-20', 'Uniform-90-10']:
                distribution_name = 'Unif' + distribution[7:]
            distribution_df.insert(loc=0, column='distribution', value=distribution_name)
            
            sim_results = pd.concat([sim_results, distribution_df], ignore_index=True)
            del distribution_df

        #Make sure the graph, weight_set and opinion_set, and time steps are integers instead of floats
        #To make sure we get an average/std with pandas, set the cluster numbers to floats instead of ints
        sim_results = sim_results.astype({'weight_set': int, 'opinion_set': int, 'T': int, 'T_changed': int,
                                         'n_clusters':float, 'n_minor':float, 'n_clusters_major':float})

        #Calculate the log of time steps to make it easier to visualize
        sim_results['log(T)'] = np.log10(sim_results['T'])
        sim_results['log(T_changed)'] = np.log10(sim_results['T_changed'])
    
        #Create a new column for distribution name and mu
        if i == 0:  #For the constant distribution
            sim_results['curve_name'] = r"$m=$" + sim_results['mu'].astype(str)
        else:
            if single_mu:
                sim_results['curve_name'] = sim_results['distribution']
            else:
                # sim_results['curve_name'] = sim_results['distribution'] + r",  $\mu=$" + sim_results['mu'].astype(str)
                sim_results['curve_name'] = sim_results['distribution'].str[-5:] + r" Mean, $m=$" + sim_results['mu'].astype(str)
            
        if not show_std:
            sim_results = sim_results.groupby(['curve_name', 'd'], as_index=False).mean()
    
    
        #Drop the rows of df with c values we don't want to include in the plot
        sim_results = sim_results[~sim_results['d'].isin(drop_ds)]
    
        #Generate line plot for this distribution group
        if distribution_subset == ['Constant']:
            sns.lineplot(x ='d', y = key, ax = ax, data = sim_results, errorbar='sd',
                         hue = "curve_name",  palette = constant_distribution_colorlist,
                         style = "curve_name", markers = constant_markerlist, dashes = constant_dashlist, 
                         markersize=8)
        else:
            if single_mu:
                hue_order = distribution_subset.copy()
                for index in range(len(hue_order)):
                        distribution = hue_order[index]
                        if distribution in ['Uniform-80-43', 'Uniform-80-20', 'Uniform-90-10']:
                            hue_order[index] = 'Unif' + distribution[7:]
            else:
                hue_order = []
                for distribution in distribution_subset:
                    for mu in mus:
                        title = distribution[-5:] + r" Mean, $m=$" + f"{mu}"
                        hue_order.append(title)
                        
            sns.lineplot(x ='d', y = key, ax = ax, data = sim_results, errorbar='sd',
                         hue = "curve_name",  palette = colorlist, hue_order = hue_order,
                         style = "curve_name", markers = markerlist, dashes = dashlist, markersize=8)
        
        #Hide the default seaborn legend
        ax.get_legend().remove()
        
        if i == 0: #Constant plot
            legend = ax.legend(title = "", loc='best', 
                                fontsize = fontsizes['S'], title_fontsize = fontsizes['M'], 
                                markerscale = 2)
        if single_mu and len(group_names) == 2:
            legend = ax.legend(title = "", loc='best', 
                                fontsize = fontsizes['S'], title_fontsize = fontsizes['M'], 
                                markerscale = 2)
        elif i == 1: #Legend off to the side of the means of the weight distributions
            handles, labels = ax.get_legend_handles_labels()
            legend = fig.legend(handles=handles, labels=labels,
                                title = "", loc='center left', bbox_to_anchor=(1.0, 0.5),
                                fontsize = fontsizes['S'], title_fontsize = fontsizes['M'], markerscale=2)
            # fig.legend(handles=handles[3:], labels=labels[3:])
        
        title = group_names[i]
        if title in ['80-43', '80-20', '90-10']:
            title = title + " Distributions"
        ax.set_title(title, fontsize = fontsizes['L'])

        # ax.set_xlabel(r"Initial confidence ($c$)", fontsize = fontsizes['M'])
        # ax.set_ylabel(plot_title[key], fontsize = fontsizes['M'])
        ax.set(xlabel=None)
        ax.set(ylabel=None)
        ax.tick_params(axis = 'both', labelsize = fontsizes['S'])
        ax.set_xlim(min_d - 0.01, max_d + 0.01)
        if custom_xticks:
            ax.set_xticks(xtick_list)

    # Large axis labels
    fig.supylabel(plot_title[key], fontsize = fontsizes['XL'], va='center', ha='center')
    fig.supxlabel(r"Confidence bound ($c$)", fontsize = fontsizes['XL'])
    
    #Large supertitle
    if show_suptitle:
        suptitle = "" + " "*sup_title_spaces
        if graph_type == "complete":
            suptitle = title_prefix[stat] + plot_title[key] + " for C(" + str(n) + ")"
        elif graph_type == "erdos-renyi":
            suptitle = title_prefix[stat] + plot_title[key] + " for G(n=" + str(n) + ", p=" + str(p) + ")"
        elif graph_type == "SBM-2-community":
            suptitle = title_prefix[stat] + plot_title[key] + " for 2-Community SBM"
        elif graph_type == "SBM-core-periphery":
            suptitle = title_prefix[stat] + plot_title[key] + " for Core--Periphery SBM"
        elif graph_type == "Caltech":
            suptitle = title_prefix[stat] + plot_title[key] + " for Caltech"
        plt.suptitle(suptitle, fontsize = fontsizes['XXL'])

    #Padding between plots
    # fig.tight_layout(pad=1.0)
        
    #Save the plot
    savefile = directory
    if graph_type == "erdos-renyi":
        savefile = savefile + f"/ER{n}-p{p}"
    else:
        savefile = savefile + f"/{graph_type}{n}"
    savefile = savefile + '--' + savename_suffix + '--' + plot_name[key] + ".png"
    plt.savefig(savefile, bbox_inches='tight', facecolor='white')
    plt.show()
        
        # plt.close()