"""
Created on 3/31/2022
Created by Grace Li

This script continues simulations of our DW model with node weights on synthetic graphs. If after running 
SyntheticGraphSimulations.py, simulations reached bailout time (but their matfiles were saved), the simulations can be continued
where they left off by entering the model parameters here. This script will read the stored matfile and continue the
simulation with a longer bailout time set. The resulting continued simulation is stored in a separate "continue" matfile.
"""

"""
Note 5/2/2024: Initially we had called the distributions that had the same mean as a Pareto distribution with parameter alpha = log(0.1) / log(0.2/0.9) "80-10" distributions. However, as we detail in our erratum such distributions are actually "80-43". In this script, we have changed the names to "Pareto-80-43", "Exp-80-43", and "Uniform-80-43" so that the distribution name now correct match.
"""

# Import required packages
import numpy as np
import pandas as pd
from scipy import io
import sys
import random
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import animation
import seaborn as sns
import time as time
import igraph as igraph
import os.path
from os import getpid
import multiprocessing

# Make process "nicer" and lower priority
import psutil
psutil.Process().nice(4)# if on *ux

# Import our own DW module
import sys
sys.path.append('..') #look one directory above
import DW as DW

# Class for running sets of DW experiments            
class DW_experiment:
    
    #Pareto Type I Distribution scale-parameter alpha where the distribution is on [1, infty)
    pareto_a = {'Pareto-90-10': math.log(10) / math.log(9),
                'Pareto-80-20': math.log(5) / math.log(4),
                'Pareto-80-43': math.log(10) / math.log(4.5)}
    
    #Shifted exponential distribution on [1, infty) mean beta
    exp_beta = {'Exp-80-20': 6.2125,
                'Exp-90-10': 20.8543,
                'Exp-80-43': 1.8836}
    
    #Uniform distribution right boundary for distribution defined on [1, b]
    uniform_b = {'Uniform-80-20': 13.425,
                 'Uniform-90-10': 42.7086,
                 'Uniform-80-43': 4.7672}
    
    #Tolerance, and max timestep values
    tol = 0.02
    # Tmax = 10**8
    
    random_graph_types = ["erdos-renyi", "SBM-2-community", "SBM-core-periphery"]
    
    folder_names = {'complete': 'Complete', 'erdos-renyi': 'Erdos-Renyi', 'SBM-2-community': 'SBM', 'SBM-core-periphery': 'SBM'}
    
    # Initialize class with graph_type and number of nodes n
    def __init__(self, graph_type, n, distribution, Tmax = 10**8, continue_round = 0,
                 p=False, pref_matrix=False, block_sizes=False, 
                 return_opinion_series = False, opinion_timestep = 1):
        '''
        Initializes class to run Deffaunt-Weisbuch simulations for a particular synthetic graph type
        
        Parameters
        ----------
        graph_type : string
            String specifying the graph type. Currently the options are "complete", "erdos-renyi",
            "SBM-2-community" and "SBM-core-periphery"
        distribution : string
            String specifying the distribution of node weights.
            Currently, the options are "Constant", "Pareto-80-43", "Pareto-80-20", "Pareto-90-10", 
            "Exp-80-43", "Exp-80-20", "Exp-90-10", "Uniform-80-43", "Uniform-80-20", "Uniform-90-10"
        n : int
            Number of nodes in the graph(s) considered
        Tmax : int, default = 10**8
            Bailout time for the simulations
        continue_round : int, default = 0
            How many time we are willing to continue the simulation from where left off.
            This is so we know were to look for past simulation files to continue from
            By default continue_round is 0 and we don't won't continue from any past simulation files
        p : float, required only if graph_type == "erdos-renyi"
            If the graph_type is "erdos-renyi", then p is a required parameter. 
            p is the edge probability in the G(n,p) Erdos-Renyi model
        block_sizes : list 
            If the graph_type is an SBM, then block_sizes is a required parameter. 
            It is a list of the number of vertices in each block of the SBM adjacency matrix.
            See the igraph.Graph.SBM function for more details.
        pref_matrix : list of lists
            If the graph_type is an SBM, then pref_matrix is a required parameter. 
            It a matrix (in the form of a list of lists) of edge probability between/within each block in the SBM.
            See the igraph.Graph.SBM function for more details.
        return_opinion_series : bool, default = False
            Specify whether or not to generate and store the opinon trajectories over time
        opinion_timestep : int, default = 1
            If return_opinion_series is True, then this sets how often we get the opinion and add it to the timeseries
        '''
        
        self.graph_type = graph_type
        self.n = n
        self.distribution = distribution
        self.Tmax = Tmax
        self.continue_round = continue_round
        
        #savefolder name for experiment
        self.foldername = self.folder_names[graph_type] + "/" + graph_type + str(n) + "/" + self.distribution
        
        #Check that the savefolder directory exists, and if not, create it
        if not os.path.exists(self.foldername):
            os.makedirs(self.foldername)
            os.makedirs(self.foldername + '/matfiles')
            os.makedirs(self.foldername + '/txtfiles')
            os.makedirs(self.foldername + '/combined_matfiles')
            os.makedirs(self.foldername + '/plots')
            if self.graph_type in ["erdos-renyi"]:
                os.makedirs(self.foldername + '/sim_seeds')
        
        if self.graph_type == "erdos-renyi":
            self.p = p
            #TODO If p == False raise error
        if self.graph_type in ["SBM-2-community", "SBM-core-periphery"]:
            self.block_sizes = block_sizes
            self.pref_matrix = pref_matrix
            #TODO If block_sizes == False or pref_matrix == False raise error
        
        #Set weight distribution parameter
        if self.distribution in self.pareto_a.keys():
            self.a = self.pareto_a[self.distribution]
        elif self.distribution in self.exp_beta.keys():
            self.beta = self.exp_beta[self.distribution]
        elif self.distribution in self.uniform_b.keys():
            self.b = self.uniform_b[self.distribution]
         
        #Set up opinion series parameters and save folder if applicable
        self.return_opinion_series = return_opinion_series
        self.opinion_timestep = opinion_timestep
        if self.return_opinion_series:
            if not os.path.exists(self.foldername + '/opinion_series'):
                os.makedirs(self.foldername + '/opinion_series')
        
    def generate_seed_files(self):
        '''
        Generate and save random seed files for randome graphs (if not complete), weights, and initial opinions if they don't exist yet
        '''
        
        self.graph_seed_file = self.folder_names[graph_type] + "/" + self.graph_type + str(self.n) + "/graph_seeds.csv"
        self.weight_seed_file = self.folder_names[graph_type] + "/" + self.graph_type + str(self.n) + "/weight_seeds.csv"
        self.opinion_seed_file = self.folder_names[graph_type] + "/" + self.graph_type + str(self.n) + "/opinion_seeds.csv"

        if self.graph_type == "complete":
                
            #There is only one weight seed for a complete graph, so we generate and save it if it doesn't exist yet
            if not os.path.exists(self.weight_seed_file):
                df = pd.DataFrame(columns = ['weight_seed'])
                random.seed(a=None) #reset random by seeding it with the current time
                weight_seed = str(random.randrange(sys.maxsize))
                df.loc[0] = [weight_seed]
                df.to_csv(self.weight_seed_file, index=False, header=True)
            
            if not os.path.exists(self.opinion_seed_file):
                df = pd.DataFrame(columns = ['weight_set', 'opinion_seed'])
                df.to_csv(self.opinion_seed_file, index=False, header=True)
         
        elif self.graph_type == "erdos-renyi":
            
            #There is one graph seed per p value for an erdos-renyi graph, so we generate and save it if it doesn't exist yet
            if not os.path.exists(self.graph_seed_file):
                df = pd.DataFrame(columns = ['p', 'graph_seed'])
                df.to_csv(self.graph_seed_file, index=False, header=True)
            df = pd.read_csv(self.graph_seed_file)
            row = df[df['p'] == self.p]
            if len(row) == 0:
                random.seed(a=None) #reset random by seeding it with the current time
                graph_seed = str(random.randrange(sys.maxsize))
                row = pd.DataFrame(columns = ['p', 'graph_seed'])
                row.loc[0] = [self.p, graph_seed]
                df = df.append(row, ignore_index=True)
                df.to_csv(self.graph_seed_file, index=False, header=True)
            
            if not os.path.exists(self.weight_seed_file):
                df = pd.DataFrame(columns = ['p', 'graph', 'weight_seed'])
                df.to_csv(self.weight_seed_file, index=False, header=True)
            
            if not os.path.exists(self.opinion_seed_file):
                df = pd.DataFrame(columns = ['p', 'graph', 'weight_set', 'opinion_seed'])
                df.to_csv(self.opinion_seed_file, index=False, header=True)
                
        elif self.graph_type in ["SBM-2-community", "SBM-core-periphery"]:
            
            #There is only one graph seed per type of SBM, so we generate and save it if it doesn't exist yet
            if not os.path.exists(self.graph_seed_file):
                df = pd.DataFrame(columns = ['graph_seed'])
                df.to_csv(self.graph_seed_file, index=False, header=True)
            df = pd.read_csv(self.graph_seed_file)
            if len(df) == 0:
                random.seed(a=None) #reset random by seeding it with the current time
                graph_seed = str(random.randrange(sys.maxsize))
                row = pd.DataFrame(columns = ['graph_seed'])
                row.loc[0] = [graph_seed]
                df = df.append(row, ignore_index=True)
                df.to_csv(self.graph_seed_file, index=False, header=True)
            
            if not os.path.exists(self.weight_seed_file):
                df = pd.DataFrame(columns = ['graph', 'weight_seed'])
                df.to_csv(self.weight_seed_file, index=False, header=True)
            
            if not os.path.exists(self.opinion_seed_file):
                df = pd.DataFrame(columns = ['graph', 'weight_set', 'opinion_seed'])
                df.to_csv(self.opinion_seed_file, index=False, header=True)
            
        #File where the random seeds for simulation are stored
        if self.graph_type == 'erdos-renyi':
            self.sim_seed_file = self.foldername + '/sim_seeds/p-' + str(self.p) + '.csv'
        else:
            self.sim_seed_file = self.foldername + '/sim_seeds.csv'
        
        #If sim seed file doesn't already exist, create it
        if not os.path.exists(self.sim_seed_file):
            df = pd.DataFrame(columns = ['d', 'mu', 'weight_set', 'opinion_set', 'sim_seed'])
            if self.graph_type == "erdos-renyi":
                df.insert(0,'graph_number','')
                df.insert(0,'p','')
            if self.graph_type[:3] == "SBM":
                df.insert(0,'graph_number','')
            df.to_csv(self.sim_seed_file, index=False, header=True)
            
        return
    
    ## Function to Run DW model for this graph and weight/opinion seeds  
    def run_DW(self, params):
    
        '''
        Runs DW experiment and saves appropriate output files
        Takes in a dictionary params, containing
        "continue_sim" - whether or not we're continuing from a past simulation
        (in this case we also need that filename "previous_matfile"),
        "d" - the confidence bound, "mu" - the compromise parameter,
        and "weight_set" and "opinion_set" - integers representing which sets in
        this overall experiment on the graph G with weight/opinion seeds
        to run the DW model on. For a complete we only need these parameters.
        For random-graph models (Erdos-Renyi and SBM), the extra parameter, "graph_number" 
        needs to be specified, and it represents which randomly generated graph to consider.
        '''
        
                
        ## Initial set up
        #Unpack parameters
        continue_sim = params["continue_sim"]
        if self.continue_round == 0:
            continue_sim = False
        if continue_sim:
            previous_matfile = params["previous_matfile"]
            
        d, mu = params["d"], params["mu"]
        opinion_set = params["opinion_set"]
        weight_set = params["weight_set"]
        if self.graph_type in self.random_graph_types:
            graph_number = params["graph_number"]
        
        
        #Print some parameters so we know what we're running
        print('Process Number ', getpid())
        if continue_sim:
            print('Continuing matfile', previous_matfile)
        else:
            print('Params', params)
            
        ## Read the random seeds if they exist, and generate and store them if they don't exist yet
        lock.acquire()
        
        #If we're continuing the simualtion, check that the savefolder directory for txt and mat files exists, and if not, create them
        if continue_sim:
            directories = [self.foldername + '/txtfiles/continue' + str(self.continue_round),
                           self.foldername + '/matfiles/continue' + str(self.continue_round)]
            if self.graph_type in self.random_graph_types:
                directories = [self.foldername + '/txtfiles/continue' + str(self.continue_round) + '/graph' + str(graph_number) + '/',
                               self.foldername + '/matfiles/continue' + str(self.continue_round) + '/graph' + str(graph_number) + '/']
            if self.graph_type == 'erdos-renyi':
                directories = [( self.foldername + '/txtfiles/continue' + str(self.continue_round) + '/' 
                                 + 'p' + str(self.p) + '/graph' + str(graph_number) + '/' ),
                               ( self.foldername + '/matfiles/continue' + str(self.continue_round) + '/'
                                 + 'p' + str(self.p) + '/graph' + str(graph_number) + '/')]
            for directory in directories:
                if not os.path.exists(directory):
                    os.makedirs(directory)
                
        #Otherwise, for random graphs, we still need to check that the appropriate subfolders exist
        elif self.graph_type in self.random_graph_types:       
            directories = [self.foldername + '/txtfiles/graph' + str(graph_number) + '/',
                       self.foldername + '/matfiles/graph' + str(graph_number) + '/']
            if self.graph_type == 'erdos-renyi':
                directories = [self.foldername + '/txtfiles/p' + str(self.p) + '/graph' + str(graph_number) + '/',
                               self.foldername + '/matfiles/p' + str(self.p) + '/graph' + str(graph_number) + '/']
            for directory in directories:
                if not os.path.exists(directory):
                    os.makedirs(directory)
        
        # Get the random graph seed if not a complete graph
        if self.graph_type in self.random_graph_types:
            df = pd.read_csv(self.graph_seed_file)
            if self.graph_type == 'erdos-renyi':
                df = df[df['p'] == self.p]
            graph_seed = df['graph_seed'].values[0]
            graph_seed = int(graph_seed)
            
        # Get the random weight set seed
        df = pd.read_csv(self.weight_seed_file)
        if self.graph_type == 'complete':
            weight_seed = df['weight_seed'].values[0]
        elif self.graph_type in self.random_graph_types:
            row = df[df['graph'] == graph_number]
            if self.graph_type == 'erdos-renyi':
                row = row[row['p'] == self.p]
            if len(row) == 0:
                random.seed(a=None) #reset random by seeding it with the current time
                weight_seed = random.randrange(sys.maxsize)
                if self.graph_type == 'erdos-renyi':
                    row = pd.DataFrame(columns = ['p', 'graph', 'weight_seed'])
                    row.loc[0] = [self.p, graph_number, str(weight_seed)]
                else:
                    row = pd.DataFrame(columns = ['graph', 'weight_seed'])
                    row.loc[0] = [graph_number, str(weight_seed)]
                df = df.append(row, ignore_index=True)
                df.to_csv(self.weight_seed_file, index=False, header=True)
            else:
                weight_seed = row['weight_seed'].values[0]
                weight_seed = int(weight_seed)
                
        # Get the random opinion set seed 
        df = pd.read_csv(self.opinion_seed_file)
        row = df[df['weight_set'] == weight_set]
        if self.graph_type in self.random_graph_types:
            row = row[row['graph'] == graph_number]
        if self.graph_type == 'erdos-renyi':
            row = row[row['p'] == self.p]
        if len(row) == 0:
            random.seed(a=None) #reset random by seeding it with the current time
            opinion_seed = random.randrange(sys.maxsize)
            if self.graph_type == 'complete':
                row = pd.DataFrame(columns = ['weight_set', 'opinion_seed'])
                row.loc[0] = [weight_set, str(opinion_seed)]
            elif self.graph_type == 'erdos-renyi':
                row = pd.DataFrame(columns = ['p', 'graph', 'weight_set', 'opinion_seed'])
                row.loc[0] = [self.p, graph_number, weight_set, str(opinion_seed)]
            elif self.graph_type in self.random_graph_types:
                row = pd.DataFrame(columns = ['graph', 'weight_set', 'opinion_seed'])
                row.loc[0] = [graph_number, weight_set, str(opinion_seed)]
            df = df.append(row, ignore_index=True)
            df.to_csv(self.opinion_seed_file, index=False, header=True)
        else:
            opinion_seed = row['opinion_seed'].values[0]
            opinion_seed = int(opinion_seed)
        lock.release()
        
        ## Specify the save file names
        savename = ""
        if self.graph_type == 'erdos-renyi':
            savename = 'p' + str(self.p) + '/graph' + str(graph_number) + '/'
            savename = savename + 'p' + str(self.p) + '-graph' + str(graph_number) + "--"
        elif self.graph_type in self.random_graph_types:
            savename = 'graph' + str(graph_number) + '/graph' + str(graph_number) + "--"
        savename = savename + 'd' + str(d) + '-mu' + str(mu) + '-weight' + str(weight_set)  
        txtfile = self.foldername + '/txtfiles/' + savename + '.txt'
        if continue_sim:
            txtfile = self.foldername + '/txtfiles/continue' + str(self.continue_round) + '/' + savename + '.txt'
        
        ## If the txtfile doesn't exist yet, create it and write the header with seed values to it
        lock.acquire()
        if not os.path.exists(txtfile):
            print(txtfile)
            with open(txtfile, 'w') as f:
                print('Experiment:', self.graph_type, ", n =", self.n, file=f, flush=True)
                if self.graph_type in self.random_graph_types:
                    print('graph_seed = ', graph_seed, file=f, flush=True)
                if self.graph_type == 'erdos-renyi':
                    print("p =", self.p, ", graph_number = ", graph_number, file=f, flush=True)
                print('d = ', d, ' and mu = ', mu, file=f, flush = True)
                print('weight_set = ', weight_set, file=f, flush = True)
                print('weight_seed = ', weight_seed, file=f, flush = True)
                print('opinion_seed = ', opinion_seed, file=f, flush = True)
        lock.release()
        
        ## Generate graph
        if self.graph_type == "complete":
            G = igraph.Graph.Full(self.n)
        elif self.graph_type == "erdos-renyi":
            #Reinitialize the random seed and generate the corresponding graph number from that seed
            random_graph = np.random.default_rng(graph_seed)
            for i in range(graph_number + 1):
                seed = random_graph.integers(low=0, high=sys.maxsize)
                random.seed(a=seed)
                G = igraph.Graph.Erdos_Renyi(self.n, self.p)
        elif self.graph_type[:3] == 'SBM':
            #Reinitialize the random seed and generate the corresponding graph number from that seed
            random_graph = np.random.default_rng(graph_seed)
            for i in range(graph_number + 1):
                seed = random_graph.integers(low=0, high=sys.maxsize)
                random.seed(a=seed)
                G = igraph.Graph.SBM(self.n, self.pref_matrix, self.block_sizes, directed=False, loops=False)
           
        ## Set the node weights 
        #Check to see if the graph has any isolated nodes and set their weight to 0 so they are never drawn.
        #Get the components of the graph
        component_membership = G.clusters(mode='strong').membership
        n_components = max(component_membership)+1
        isolated_nodes = []
        for i in range(n_components):
            component = np.nonzero(np.array(component_membership)==i)[0]
            if len(component) == 1:
                isolated_nodes.append(component[0])

        ## Get the weights and opinions for weight_set and opinion_set and set nodes of G to those values
        #If a constant-weight experiment, set all weights to 1
        if self.distribution == 'Constant':
            G.vs['weight'] = [1] * self.n
        #Otherwise, for a Pareto distribution, reinitialize the random seed and generate the corresponding weight set from that seed
        elif self.distribution in self.pareto_a.keys():
            random_weight = np.random.default_rng(weight_seed)
            for i in range(weight_set + 1):
                weights = random_weight.pareto(self.a, size=self.n) + 1
            G.vs['weight'] = weights
        #Same for an exponential distribution
        elif self.distribution in self.exp_beta.keys():
            random_weight = np.random.default_rng(weight_seed)
            for i in range(weight_set + 1):
                weights = random_weight.exponential(scale=self.beta, size=self.n) + 1
            G.vs['weight'] = weights
        #Same for a uniform distribution
        elif self.distribution in self.uniform_b.keys():
            random_weight = np.random.default_rng(weight_seed)
            for i in range(weight_set + 1):
                weights = random_weight.uniform(low=1, high=self.b, size=self.n)
            G.vs['weight'] = weights
            
        #Set any isolated nodes to have weight 0
        if n_components > 1:
            G.vs[isolated_nodes]['weight'] = 0
            
        ## Set the initial opinions: Reinitialize the random seed and generate the corresponding opinion set from that seed
        random_opinion = np.random.default_rng(opinion_seed)
        for i in range(opinion_set + 1):
            init_opinions = random_opinion.uniform(0, 1, size=self.n)
        G.vs['opinion'] = init_opinions
        
        ## Read or create a simulation seed for DW node selection for this set of parameters (d, mu, weight_set, opinion_set)
        #Read the random seed csv file as a pandas dataframe
        lock.acquire()
        df = pd.read_csv(self.sim_seed_file)
        
        #Try to get the corresponding dataframe row for this simulation
        row = df[df['d'] == d]
        row = row[row['mu'] == mu]
        row = row[row['weight_set'] == weight_set]
        row = row[row['opinion_set'] == opinion_set]
        if self.graph_type in self.random_graph_types:
            row = row[row['graph_number'] == graph_number]
        if self.graph_type == "erdos_renyi":
            row = row[row['p'] == self.p]
            
        #If there isn't already an entry for this simulation generate and store a simulation seed
        if len(row) == 0:
            random.seed(a=None) #reset random by seeding it with the current time so we don't keep generating the same sim seeds
            sim_seed = random.randrange(sys.maxsize)
            row = pd.DataFrame(columns = ['d', 'mu', 'weight_set', 'opinion_set', 'sim_seed'])
            row.loc[0] = [d, mu, weight_set, opinion_set, str(sim_seed)]
            if self.graph_type in self.random_graph_types:
                row.insert(0,'graph_number', graph_number)
            if self.graph_type == "erdos-renyi":
                row.insert(0,'p', self.p)
            df = df.append(row, ignore_index=True)
            df.to_csv(self.sim_seed_file, index=False, header=True)
        else:
            # print('Process Number ', getpid(), 'row has a sim_seed entry') #delete line
            sim_seed = row['sim_seed'].values[0]
            if len(row) > 1:
                with open(txtfile, 'w') as f:
                    print("OH NO! Something went wrong and there are multiple sim_seeds", file=f, flush=True)
                    print(row, file=f, flush=True)
        lock.release()
            
        #Read the results of the previous simulation if we are continuing from a previous file
        if continue_sim:
            previous_results = io.loadmat(previous_matfile)
            G.vs['opinion'] = previous_results['final_opinions'][0] #Continue from the opinions where we left off
            
        #Time the DW simulation for this weight + opinion set combo
        start_time = time.time()
        
        ## Run the DW model using the simulation seed
        if continue_sim:
            outputs = DW.DW(G, d, mu, random_seed = sim_seed, t_start = previous_results['T'][0][0], Tmax = self.Tmax,
                            return_opinion_series = self.return_opinion_series, opinion_timestep = self.opinion_timestep)
        else:
            ## Run the DW model using the simulation seed
            outputs = DW.DW(G, d, mu, random_seed = sim_seed, Tmax = self.Tmax,
                            return_opinion_series = self.return_opinion_series, opinion_timestep = self.opinion_timestep)

        # Dump model outputs into fileW
        lock.acquire()
        with open(txtfile, 'a') as f:
            print("\n----- Weight Set %s and Opinion Set %s -----" % (weight_set, opinion_set), file=f, flush=True)

            print("T = %s" % outputs['T'], file=f, flush=True)
            print("T_changed = %s" % outputs['T_changed'], file=f, flush=True)
            print("Number of Clusters = %s" % outputs['n_clusters'], file=f, flush=True)

            print("Cluster Membership", file=f, flush=True)
            print(outputs['clusters'], file=f, flush=True)
            
            runtime = time.time() - start_time
            print('-- Runtime was %.0f seconds = %.3f hours--' % (runtime, runtime/3600) , file=f, flush=True)
        lock.release()

        ## Define dictionary to store simulation outputs for saving to a .mat file
        save_sim = {'d': d, 'mu': mu, 'weight_set': weight_set, 'opinion_set': opinion_set, 'sim_seed': sim_seed}
      
        #Include the graph-level results
        save_sim['T'] = outputs['T']                    
        save_sim['T_changed'] = outputs['T_changed']
        if continue_sim:
            save_sim['T_changed'] = outputs['T_changed'] + previous_results['T_changed'][0][0]
        save_sim['bailout'] = outputs['bailout']
        save_sim['avg_opinion_diff'] = outputs['avg_opinion_diff']

        #Include the cluster information
        clusters = outputs['clusters']
        save_sim['n_clusters'] = outputs['n_clusters']
        for i in range(outputs['n_clusters']):
            key = 'cluster' + str(i)
            save_sim[key] = clusters[i]
            #clusters can be extracted from matfile using list = clusteri.flatten().tolist()

        #Include the node-level results as size n arrays
        save_sim['weights'] = G.vs['weight']
        save_sim['init_opinions'] = init_opinions
        save_sim['final_opinions'] = outputs['final_opinions']
        save_sim['total_change'] = outputs['total_change']
        save_sim['n_updates'] = outputs['n_updates']
        if continue_sim:
            save_sim['total_change'] = outputs['total_change'] + previous_results['total_change'][0]
            save_sim['n_updates'] = outputs['n_updates'] + previous_results['n_updates'][0][0]
        save_sim['local_agreement'] = outputs['local_agreement']
        save_sim['local_receptiveness'] = outputs['local_receptiveness']
        
        ## Save the simulation results to a matfile
        matfile = self.foldername + '/matfiles/' + savename + '-op' + str(opinion_set) +'.mat'
        if continue_sim:
            matfile = self.foldername + '/matfiles/continue' + str(self.continue_round) + '/' + savename + '-op' + str(opinion_set) +'.mat'
        io.savemat(matfile, save_sim)
    
        #If we generate the opinion trajectories, then get those too and save to an opinion_trajectories folder
        if self.return_opinion_series:
            save_sim['opinion_series'] = outputs['opinion_series']
            save_sim['opinion_timestep'] = self.opinion_timestep
            del save_sim['init_opinions']
            del save_sim['final_opinions']
            matfile = self.foldername + '/opinion_series/' + savename + '-op' + str(opinion_set) +'.mat'
            if continue_sim:
                matfile = self.foldername + '/opinion_series/continue' + str(self.continue_round) + '/' + savename + '-op' + str(opinion_set) +'.mat'
            io.savemat(matfile, save_sim)
    
def init(l):
    global lock
    lock = l
    

if __name__ == "__main__":

    ## EXPERIMENT PARAMETERS - CHANGE HERE
    Tmax = 10**9 #Bailout time for simulations
    continue_round = 1 #how many times we've tried to continue simulations (for saving files)
    
    graph_type = 'complete'
    n = 500 #number of nodes
    
    #Names of distributions for the node weights
    distributions = ['Constant', 'Pareto-80-43', 'Pareto-80-20',
                     'Uniform-80-43', 'Uniform-80-20',
                     'Exp-80-43', 'Exp-80-20']
    distributions = ['Pareto-90-10', 'Exp-90-10', 'Uniform-90-10']
    
    '''
    NOTE: THE CONFIDENCE BOUND IS CALLED d HERE, BUT c IN THE PAPER
    NOTE: THE COMPROMISE PARAMETER IS CALLED mu HERE, BUT m IN THE PAPER
    '''
    #confidence bound
    ds = [0.1, 0.2, 0.25, 0.26, 0.27, 0.28, 0.29, 0.3, 0.35, 0.4, 0.5, 0.7, 0.9]
    #compromise parameter
    mus = [0.1, 0.3, 0.5]
    
    #By default we don't need these parameters unless its for the relevent random-graph model
    p = False
    pref_matrix = False 
    block_sizes = False
    
    # Edge probability p for the Erdos-Renyi  G(n,p) model. Change here:
    if graph_type == "erdos-renyi":
        p = 0.5
    
    # Change SBM parameters here:
    #Block sizes
    A = int(n * 0.75)
    B = int(n * 0.25)
    block_sizes = [A, B]

    #Edge probabilities
    if graph_type == "SBM-2-community":
        p = 0.1 #ER probability for n to match the mean degree of
        p_aa = (n - 1) / (A - 1) * p
        p_bb = (n - 1) / (B - 1) * p
        p_ab = 1 / n
        pref_matrix = [[p_aa, p_ab], [p_ab, p_bb]]
    elif graph_type == "SBM-core-periphery":
        p = 0.3 #ER probability for n to match the mean degree of
        p_aa = (n - 1) / (A - 1) * p
        p_ab = 20 / n
        p_bb = 1 / (B - 1)
        pref_matrix = [[p_aa, p_ab], [p_ab, p_bb]]

    #Specify which graphs to look at if using a random-graph model
    graph_numbers = list(range(0,5))
    if graph_type == "complete":
        graph_numbers = [0] #if we have complete graphs, there is only one graph to look at
    
    #Specify which weight and opinion sets to run
    weight_sets = list(range(0,10))
    opinion_sets = list(range(0,10))
    
    #Specify whether or not to generate the opinion series
    return_opinion_series = False
    opinion_timestep = 1
    
    folder_names = {'complete': 'Complete', 'erdos-renyi': 'Erdos-Renyi', 'SBM-2-community': 'SBM', 'SBM-core-periphery': 'SBM'}

    #Gather the paramters and run simulations for each type of node-weight distribution
    for distribution in distributions:
        ## Generate list of tuples to feed into DW_experiments as parameters
        params_list = []
        for graph_number in graph_numbers:
            for d in ds:
                for mu in mus:
                    for weight_set in weight_sets:
                        for opinion_set in opinion_sets:

                            continue_file_found = False #Whether or not we found a matfile to continue the simulation from 
                            
                            #Filenames
                            matfile_folder = folder_names[graph_type] + '/' + graph_type + str(n) + '/' + distribution + '/matfiles/'
                            if graph_type == "complete":
                                filename = 'd' + str(d) + '-mu' + str(mu) + '-weight' + str(weight_set) + '-op' + str(opinion_set) + '.mat'
                            elif graph_type in ["SBM-2-community", "SBM-core-periphery"]:
                                filename = ('graph' + str(graph_number) + '/graph' + str(graph_number)  
                                        + "--d" + str(d) + '-mu' + str(mu) 
                                        + '-weight' + str(weight_set) + '-op' + str(opinion_set) + '.mat')
                            elif graph_type == "erdos-renyi":
                                filename = ('p' + str(p) + '/graph' + str(graph_number) 
                                           + '/p'+ str(p) + '-graph' + str(graph_number)  
                                           + "--d" + str(d) + '-mu' + str(mu) 
                                           + '-weight' + str(weight_set) + '-op' + str(opinion_set) + '.mat')
                            
                            #If we're trying to continue the simulation, check for the files we left off from 
                            if continue_round >= 2:
                                for i in list(range(continue_round, 0, -1)): #range(start, stop, step)
                                    try: 
                                        matfile = matfile_folder + 'continue' + str(i) + '/' + filename
                                        results = io.loadmat(matfile)

                                        if i == continue_ronund:
                                            break
                                            
                                        elif results['bailout'][0][0]:
                                            param_dict = {"continue_sim": True,
                                                          "previous_matfile": matfile,
                                                          "d": d, "mu": mu, 
                                                          "weight_set": weight_set,
                                                          "opinion_set": opinion_set}
                                            if graph_type != "complete":
                                                param_dict["graph_number"] = graph_number
                                            params_list.append(param_dict) 
                                            continue_file_found = True
                                        break
                                    except:
                                        pass
                            
                            #If we haven't found a file to continue from yet, see if we have an original first simulation file
                            if not continue_file_found:
                                matfile = matfile_folder + filename
                                try:
                                    results = io.loadmat(matfile)
                                    if results['bailout'][0][0] and continue_round >= 1:
                                        param_dict = {"continue_sim": True,
                                                      "previous_matfile": matfile,
                                                      "d": d, "mu": mu, 
                                                      "weight_set": weight_set,
                                                      "opinion_set": opinion_set}
                                        if graph_type != "complete":
                                            param_dict["graph_number"] = graph_number
                                        params_list.append(param_dict) 
                                        continue_file_found = True
                                except:
                                    param_dict = {"continue_sim": False,
                                                  "d": d, "mu": mu, 
                                                  "weight_set": weight_set, 
                                                  "opinion_set": opinion_set}
                                    if graph_type != "complete":
                                        param_dict["graph_number"] = graph_number
                                    params_list.append(param_dict) 

        #Initialize experiment class
        experiment = DW_experiment(graph_type, n, distribution, Tmax = Tmax, continue_round = continue_round, 
                                   p = p, block_sizes = block_sizes, pref_matrix = pref_matrix,
                                   return_opinion_series = return_opinion_series, opinion_timestep = opinion_timestep)
        experiment.generate_seed_files()

        l = multiprocessing.Lock()

        with multiprocessing.Pool(processes=10, initializer=init, initargs=(l,)) as pool:
            pool.map(experiment.run_DW, params_list)