"""
Created on 11/13/2021
Created by Grace Li

This script calculates the results from our simulations of our adaptive-confidence HK model. Run this script after
running simulations using either SyntheticGraphSimulations.py or Caltech/CaltechSimulations.py

After running this script, the Heatmaps or Lineplot scripts can be used to generate plots of our simulation results.

To run this script, at the top, modifiy the variables that give the desired graph, number of nodes, node-weight distribution, random graph 
parameters (if applicable), and model parameters (compromise parameter mu (m in the paper) and confidence bound d (c in the paper))

For each simulation, the
    - node-weight distribution name
    - graph type/name
    - random-graph parameters (if applicable)
    - graph number (for random graphs)
    - confidence bound d (called c in the paper)
    - compromise parameter mu (called m in the paper)
    - weight set
    - opinion set
are used to load the stored matfile for that simulation.

The following quantities are calculated at convergence time for each simulation:
    - number of opinion clusters
    - number of major opinion clusters
    - number of minor opinion clusters
    - convergence time (as a number of time steps)
    - number of time steps opinions were changed
    - whether or not the bailout time is reached (as a boolean)
    - mean local receptiveness
    - Shannon entropy H
    - mean, variance, skew and kurtosis of opinions
We store these calculated results as a row in a csv file. For each node-weight distribution, and random graph parameters (if applicable),
we generate and save a csv file of simulation results and store it in the "combined_matfiles" folder for that graph. 

IMPORTANT: This script requires igraph version 0.9.7 as the syntax of that version is used to calculate our various quantities.

"""  

"""
Note 5/2/2024: Initially we had called the distributions that had the same mean as a Pareto distribution with parameter alpha = log(0.1) / log(0.2/0.9) "80-10" distributions. However, as we detail in our erratum such distributions are actually "80-43". In this script, we have changed the names to "Pareto-80-43", "Exp-80-43", and "Uniform-80-43" so that the distribution name now correct match.
"""

import numpy as np
import pandas as pd
from scipy import io, stats
import os
import random
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import animation
import seaborn as sns
import time as time
import igraph as igraph

# Import our own DW module
import sys
sys.path.append('..') #look one directory above
import DW as DW

#Change parameters here ------------------------------------------------------------

#Options for graph_type are complete, erdos-renyi, SBM-2-community, SBM-core-periphery, Caltech
graph_type = "complete"
ns = [500] #number of nodes in synthetic graph
# ns = [10, 20, 30, 45, 65, 100, 150, 200, 300, 400, 600, 700, 800, 900, 1000]
ns = [10, 20, 30, 45, 65]

max_times_continued = 2 #maximum number of times we check if we continued the simulation from a previous save file

ps = [0.1, 0.3, 0.5, 0.7] #independent edge probabilities for erdos-renyi graphs

#specify the numbers of the random graphs for erdos-renyi and SBM
graphs = list(range(5))

#Default weight distributions that we consider
# base_distributions = ["Constant", "Uniform-80-10", "Uniform-80-20", 
#                      "Exp-80-10", "Exp-80-20",
#                      "Pareto-80-10", "Pareto-80-20"]
base_distributions = ["Constant", "Uniform-80-43", "Uniform-80-20", 
                     "Exp-80-43", "Exp-80-20",
                     "Pareto-80-43", "Pareto-80-20"]

#Base set of confidence bound d we considered
'''
NOTE: THE CONFIDENCE BOUND IS CALLED d HERE, BUT c IN THE PAPER
'''
base_ds = [0.1, 0.2, 0.25, 0.3, 0.35, 0.4, 0.5, 0.7, 0.9]

#Base set of compromise parameters mu we considered
'''
NOTE: THE COMPROMISE PARAMETER IS CALLED mu HERE, BUT m IN THE PAPER
'''
base_mus = [0.1, 0.3, 0.5]

#specify weights and opinions sets
weight_sets = list(range(10))
opinion_sets = list(range(10))

#End changing parameters. Any special modications from base_ds and base_mus can be done in the for n loop below ------------------

#Name of experiment folder
folder_name_dict = {"complete": "Complete", "erdos-renyi": "Erdos-Renyi",
                    "SBM-2-community": "SBM", "SBM-core-periphery": "SBM",
                    "Caltech": "Caltech"}
folder_name = folder_name_dict[graph_type]

random_graph_folders = ["SBM", "Erdos-Renyi"] #The names of the folders with random-graph models

#Some setup in case we didn't manually change all the parameters
if folder_name == "Caltech":
    ns = [762] #we only have one possible n value for Caltech and its 762 nodes in the largest connected component
if folder_name not in random_graph_folders:
    graphs = [1] #we don't have multiple graphs to consider if it's not a random-graph model
if folder_name != "Erdos-Renyi":
    ps = [1] #we have no p-values for non ER networks and don't consider it

## Some initial set up
#Get number of weight and opinion sets
n_graphs = len(graphs)
n_weight_sets = len(weight_sets)
n_opinion_sets = len(opinion_sets)

#Loop through each graph size -------------------------------------------------------------------------
for n in ns:
    
    #Calculate the threshold size for minor clusters
    minor_threshold = math.floor(n * 2 / 100)  #For cluster sizes <= minor threshold, we consider them minor clusters and calculate a separate entropy
    print('\nn = ', n)
    print('The minor threshold is: ', minor_threshold)
    
    #Reset the weight distribution names, confidence bounds ds, and compromise parameters mus to the default values
    distributions = base_distributions
    ds = base_ds
    mus = base_mus
    
    #For some special cases were we run more or less distributions, ds, mus, etc. change the parameters here
    if folder_name == "Complete" and n == 500:
        distributions = ["Constant", "Uniform-80-43", "Uniform-80-20", "Uniform-90-10",
                         "Exp-80-43", "Exp-80-20", "Exp-90-10",
                         "Pareto-80-43", "Pareto-80-20", "Pareto-90-10"]

    if folder_name == "Complete" and n != 500:
        distributions = ["Constant", "Pareto-80-43", "Exp-80-43", "Uniform-80-43"]
        ds = [0.1, 0.3, 0.5]
        mus = [0.3, 0.5]
        
    #Name of textfile to dump the outputs
    if folder_name == "Caltech":
        txtfile = folder_name + '/consolidator_outputs.txt'
    else:
        txtfile = folder_name + '/' + graph_type + str(n) + '/consolidator_outputs.txt'
        
    ## Open a text file to dump outputs
    with open(txtfile, 'w') as f:
        print(txtfile, file=f, flush=True)
        print('The minor threshold is: ', minor_threshold, file=f, flush=True)

        #For each weight distribution and edge probability, we create a new dataframe
        for distribution in distributions:
            
            #We tried more d values for complete 500 graphs, depending on which distribution we looked at
            if folder_name == "Complete" and n == 500:
                if distribution[0] in ['E', 'U']:
                    ds = [0.1, 0.2, 0.25, 0.26, 0.27, 0.28, 0.29, 0.3, 0.35, 0.4, 0.5, 0.7, 0.9]
                elif distribution[0] in ['C', 'P']:
                    ds = [0.1, 0.2, 0.25, 0.26, 0.27, 0.28, 0.29, 0.3, 0.31, 0.32, 0.33, 0.34, 0.35, 0.36, 0.37, 0.38, 0.39, 0.4, 0.5, 0.7, 0.9]

            #Save folder name for experiment
            if folder_name == "Caltech":
                experiment = folder_name + "/" + distribution
            else:
                experiment = folder_name + "/" + graph_type + str(n) + "/" + distribution

            for p in ps:

                if folder_name == "Erdos-Renyi":
                    print('---------------------------', file=f, flush=True)
                    print('Distribution: ' + distribution + ' with p = ' + str(p))
                    print('\nDistribution: ' + distribution + ' with p = ' + str(p), file=f, flush=True)
                else:
                    print('---------------------------', file=f, flush=True)
                    print('Distribution: ' + distribution)
                    print('\nDistribution: ' + distribution, file=f, flush=True)

                total_continued = 0 #keep track of how many simulations we continued from a previous save point
                bailout_count = 0 #keep track of how many times we hit the bailout time
                file_not_found = 0 #keep a track of the count of how many files are not done yet

                #Read the stored matfiles and create a pandas dataframe of simulation results for each run
                #including number of clusters, convergence time, effective convergence time, and average opinion distance

                #Initialize dataframe
                columns = ['d', 'mu', 'weight_set', 'opinion_set',
                           'n_clusters', 'n_minor', 'n_clusters_major', 
                           'bailout', 'entropy', 'entropy_major', 
                           'avg_local_agreement', 'avg_local_receptiveness',
                           'T', 'T_changed', 'avg_opinion_diff',
                           'op_mean', 'op_var', 'op_skew', 'op_kurtosis']
                if folder_name in random_graph_folders:
                    columns = ['graph'] + columns
                    
                #Fill in the data frame row by row and store for each graph number
                for graph_number in graphs:
                    
                    if folder_name in random_graph_folders:
                        print('Graph', graph_number)
                        print('Graph', graph_number, file=f, flush=True)

                    #Initialize the dataframe
                    df = pd.DataFrame(columns = columns)

                    for d in ds:
                        for mu in mus:
                            # print('d =', d, ' and mu = ', mu, file=f, flush=True)
                            for opinion_set in opinion_sets:

                                for weight_set in weight_sets:
                                    
                                    found_file = False
                                    
                                    #Filenames
                                    if folder_name in ["Complete", "Caltech"]:
                                        filename = 'd' + str(d) + '-mu' + str(mu) + '-weight' + str(weight_set) + '-op' + str(opinion_set) + '.mat'
                                    elif folder_name == "SBM":
                                        filename = ('graph' + str(graph_number) + '/graph' + str(graph_number)  
                                                    + "--d" + str(d) + '-mu' + str(mu) 
                                                    + '-weight' + str(weight_set) + '-op' + str(opinion_set) + '.mat')
                                    elif folder_name == "Erdos-Renyi":
                                        filename = ('p' + str(p) + '/graph' + str(graph_number) 
                                                   + '/p'+ str(p) + '-graph' + str(graph_number)  
                                                   + "--d" + str(d) + '-mu' + str(mu) 
                                                   + '-weight' + str(weight_set) + '-op' + str(opinion_set) + '.mat')
                                        
                                    for i in list(range(max_times_continued, 0, -1)): #range(start, stop, step)
                                        try: 
                                            matfile = experiment + '/matfiles/continue' + str(i) + '/' + filename
                                            results = io.loadmat(matfile)
                                            print('We continued the simulation', i, ' times for ', matfile)
                                            print('We continued the simulation', i, ' times for ', matfile, file=f, flush=True)
                                            total_continued += 1
                                            break
                                        except:
                                            if i == 1:
                                                #Default name for matfile if we didn't continue the simulation for being left off somewhere
                                                matfile = experiment + '/matfiles/' + filename
                                      
                                    try:
                                        results = io.loadmat(matfile) 

                                        #Calculate the average local agreement
                                        avg_local_agreement = np.mean(results['local_agreement'][0])

                                        #Calculate the average local receptiveness
                                        avg_local_receptiveness = np.mean(results['local_receptiveness'][0])

                                        #Calculate the Shannon entropy for the normalized clusters both with and without isolated-node clusters
                                        n_clusters = results['n_clusters'][0][0]
                                        n_minor = 0
                                        #If we reached the bailout time, then there's no clusters found and we set everything to NaN
                                        if n_clusters == 0:
                                            n_clusters = np.nan
                                            n_minor = np.nan
                                            n_clusters_major = np.nan
                                            H = np.nan
                                            H_major = np.nan
                                        #If we sucessfully determined the clusters, then we calculate the Shannon entropy
                                        else:
                                            cluster_sizes = [] #get and store the size of each opinion cluster
                                            for i in range(n_clusters):
                                                key = "cluster" + str(i)
                                                cluster = results[key][0]
                                                cluster_sizes.append(len(cluster))
                                                if len(cluster) <= minor_threshold:
                                                    n_minor += 1
                                            normalized_sizes = np.array(cluster_sizes) / np.sum(np.array(cluster_sizes)) #normalize the cluster sizes
                                            #Calculate Shannon's entropy of normalized clusters
                                            H = - np.sum(normalized_sizes * np.log(normalized_sizes))

                                            #Calculate Shannon's entropy for the non-isolated clusters
                                            sizes = [size for size in cluster_sizes if size > minor_threshold]
                                            normalized_sizes = np.array(sizes) / np.sum(np.array(sizes))
                                            H_major = - np.sum(normalized_sizes * np.log(normalized_sizes))

                                            n_clusters_major = n_clusters - n_minor

                                            # if n_minor > 0:
                                            #     print('Next largest cluster size: ', min(sizes))

                                        #Calculate the mean, variance, skewness, and kurtosis of the final opinion vector
                                        final_opinions = results['final_opinions'][0]
                                        op_mean = np.mean(final_opinions)
                                        op_var = np.var(final_opinions)
                                        op_skew = stats.skew(final_opinions, axis=None)
                                        op_kurtosis = stats.kurtosis(final_opinions, axis=None, fisher=True)

                                        #Assemble the results into a dataframe row
                                        row = pd.DataFrame(columns = columns)
                                        row_list = [d, mu, weight_set, opinion_set,
                                                  n_clusters, n_minor, n_clusters_major,
                                                  results['bailout'][0][0], H, H_major, 
                                                  avg_local_agreement, avg_local_receptiveness,
                                                  results['T'][0][0], results['T_changed'][0][0],
                                                  results['avg_opinion_diff'][0][0],
                                                  op_mean, op_var, op_skew, op_kurtosis]
                                        if folder_name in random_graph_folders:
                                            row_list = [graph_number] + row_list
                                        
                                        row.loc[0] = row_list
                                        
                                        df = df.append(row, ignore_index=True)

                                        if row['bailout'][0]:
                                            print('Bailout time reached for ' +  matfile, file=f, flush=True)
                                            print('T = ',row['T'][0], ' and n_clusters = ', row['n_clusters'][0], file=f, flush=True)
                                            bailout_count += 1
                                            
                                        # print(f"d = {d}, mu = {mu}, weight {weight_set}, opinion {opinion_set}: ", cluster_sizes, file=f, flush=True) #delete
                
                                    except:
                                        print("file not found: " + matfile, file=f, flush=True)
                                        file_not_found += 1

                    #Make sure the graph, weight_set and opinion_set, n_clusterss, and time steps are integers instead of floats
                    if folder_name in random_graph_folders:
                        df = df.astype({'graph': int, 'weight_set': int, 'opinion_set': int, 'T': int, 'T_changed': int})
                    else:
                        df = df.astype({'weight_set': int, 'opinion_set': int, 'T': int, 'T_changed': int})
                    
                    #Check that a directory exists, and if not, create it
                    directory = experiment + '/combined_matfiles'
                    if folder_name == "Erdos-Renyi":
                        directory = directory + '/p' + str(p) 
                    if not os.path.exists(directory):
                        os.makedirs(directory)

                    #Save the consolidated results as a csv file
                    filename = directory
                    if folder_name in random_graph_folders:
                        filename = filename + '/graph' + str(graph_number) + '_simulation_results.csv'
                    else:
                        filename = filename + '/simulation_results.csv'
                    df.to_csv(filename, index=False, header=True)

                # print('\nDistribution: ' + distribution, file=f, flush=True)
                print('\nNumber of times we continued a simulation = ' + str(total_continued), file=f, flush=True)
                print('\nNumber of times hit bailout = ' + str(bailout_count), file=f, flush=True)
                print('Number of files not found = ' + str(file_not_found), file=f, flush=True)