"""
Created on 11/13/2021
Created by Grace Li

This script generates a figure consisting of a row of heatmaps of the results of the simulations for our DW model with node weights.

In the paper, we examine the numbers of major and minor clusters, Shannon entropy, mean local receptiveness and convergence time.
Here, we also plot the variance of final opinions and the convergence time in terms of time-steps where opinions changed.

For each quantity of interest, this script generates a figure consisting of a row of heatmaps. Each heatmap represents a single 
distribution of node weights, and has the confidence bound c (named d in the code) on the vertical axis and the compromise 
parameter m (named mu in the code) on the horizontal axis. There is an option to show labels on each cell of the heatmaps 
which can represent the mean value of our simulations +/- one standard deviation. 

For fixed graphs (i.e., complete graphs and the Caltech network), we generate each cell in each heatmap from 100 numerical
simulations from 10 sets of node weights that each have 10 sets of initial opinions. For random graphs (i.e., Erdos-Renyi and SBM graphs), 
we generate each cell in each heatmap from 500 numerical simulations from 5 random graphs each with 10 sets of node weights 
that each have 10 sets of initial opinions.

We used this script to generate our figures for our simulations of our DW model with node weights for SBM random graphs, 
and the Caltech network. For complete graphs, we examine more node-weight distributions, see Complete/Heatmaps-VerticalCbar.py.
For Erdos-Renyi random graphs, we stack the heatmaps for different edge-probability parameters, see Erdos-Renyi/Heatmaps-stacked-p.py.

"""

"""
Note 5/2/2024: Initially we had called the distributions that had the same mean as a Pareto distribution with parameter alpha = log(0.1) / log(0.2/0.9) "80-10" distributions. However, as we detail in our erratum such distributions are actually "80-43". In this script, we have changed the names to "Pareto-80-43", "Exp-80-43", and "Uniform-80-43" so that the distribution name now correct match.
"""

# Import required packages
import numpy as np
import pandas as pd
from scipy import io
import sys
import random
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import animation
import seaborn as sns
import time as time
import igraph as igraph
import os.path
from os import getpid
import multiprocessing

import DW as DW #import our own DW module

## Change parameters here ------------------------------------------------

#Options for graph_type are complete, erdos-renyi, SBM-2-community, SBM-core-periphery, Caltech
graph_type = "SBM-core-periphery"
n = 500 #number of nodes in synthetic graph
p = 0.1 #independent edge probability for erdos-renyi graphs

show_suptitle = False #whether to print a giant suptitle for the plot
show_labels = False #whether to show labels on heatmap

print_std = True #True #whether to print the mean +/- 1 standard deviation, or just the mean
if show_labels == False:
    print_std = False

#Specify which set of distributions to plot and the name for the saved plots
# distributions = ["Constant", "Pareto-80-43", "Pareto-80-20", "Pareto-90-10"]
# distribution_name = "Pareto"

# distributions = ["Constant", "Exp-80-43", "Exp-80-20", "Exp-90-10"]
# distribution_name = "Exponential"

# distributions = ["Constant", "Uniform-80-43", "Uniform-80-20", "Uniform-90-10"]
# distribution_name = "Uniform"

# distributions = ["Constant", "Uniform-80-20", "Exp-80-20", "Pareto-80-20"]
# distribution_name = "80-20"

distributions = ["Constant", "Uniform-80-43", "Uniform-80-20", #"Uniform-90-10", 
                 "Exp-80-43", "Exp-80-20", #"Exp-90-10",
                 "Pareto-80-43", "Pareto-80-20"] #"Pareto-90-10"]
distribution_name = "All"

#Specify which quantities to generate plots of
keys = [
        'log(T)', 'log(T_changed)', 
        'entropy',
        'n_minor', 'n_clusters_major',
        'avg_local_receptiveness',
        'op_var'
       ]
# keys = ['log(T)']

#specify the numbers of the random graphs for erdos-renyi and SBM
graphs = list(range(5))

#Compromise parameter
'''
NOTE: THE COMPROMISE PARAMETER IS CALLED mu HERE, BUT m IN THE PAPER
'''
mus = [0.1, 0.3, 0.5]

#Define the confidence bound sections for each distribution
'''
NOTE: THE CONFIDENCE BOUND IS CALLED d HERE, BUT c IN THE PAPER
'''
#The default values and subfigure heights
all_ds = [
            [0.1, 0.2], 
            [0.25, 0.3, 0.35, 0.4],
            [0.5, 0.7, 0.9]
         ]
subfig_heights = [2, 3, 3]

if distribution_name == "All":
    subfig_heights = [3, 4, 4]
    
#For complete graphs, we did more experiments and will plot more confidence bounds accordingly
if graph_type == "complete":
    #Default confidence bounds and subfigure heights
    all_ds = [
                [0.1, 0.2], 
                [0.25, 0.26, 0.27, 0.28, 0.29, 0.3], 
                [0.35, 0.4],
                [0.5, 0.7, 0.9]
             ]
    subfig_heights = [2, 4, 2, 2]

    if distribution_name in ['Pareto', 'Pareto-only']:
        all_ds = [
                    [0.1, 0.2], 
                    [0.25, 0.26, 0.27, 0.28, 0.29, 0.3, 0.31, 0.32, 0.33, 0.34, 0.35, 0.36, 0.37, 0.38, 0.39, 0.4],
                    [0.5, 0.7, 0.9]
                 ]
        subfig_heights = [2, 8, 3]
        
#flip ds so that heatmaps are ordered properly
all_ds = [np.flip(ds) for ds in all_ds]
all_ds.reverse()
subfig_heights.reverse()


#Name of experiment folder
folder_name_dict = {"complete": "Complete", "erdos-renyi": "Erdos-Renyi",
                    "SBM-2-community": "SBM", "SBM-core-periphery": "SBM",
                    "Caltech": "Caltech"}
folder_name = folder_name_dict[graph_type]

#Check that a directory exists, and if not, create it
directory = folder_name 
if folder_name != 'Caltech':
    directory = directory + '/' + graph_type + str(n)
directory = directory + '/combined_plots/heatmaps/' + distribution_name + ('/labeled' * show_labels) 
if not os.path.exists(directory):
    os.makedirs(directory)

## Plot parameters -----------------------------------------------------------

base_fontsizes = {'XXS': 10, 'XS': 20, 'S': 25, 'M': 30, 'L':30, 'XL': 40, 'XXL':40}
if distribution_name == 'All':
    base_fontsizes = {'XXS': 10, 'XS': 22, 'S': 35, 'M': 35, 'L': 50, 'XL': 60, 'XXL': 80}
if show_labels:
    if print_std:
        base_fontsizes = {'XXS': 10, 'XS': 20, 'S': 25, 'M': 30, 'L':35, 'XL': 40, 'XXL':40}
        if distribution_name == 'All':
            base_fontsizes = {'XXS': 10, 'XS': 22, 'S': 35, 'M': 35, 'L': 50, 'XL': 60, 'XXL': 80}
    else:
         base_fontsizes = {'XXS': 16, 'XS': 22, 'S': 35, 'M': 35, 'L': 50, 'XL': 60, 'XXL': 80}

# if folder_name == "Complete":
#     base_fontsizes = {'XS': 20, 'S': 25, 'M': 30, 'L':42, 'XL': 50, 'XXL':60}
#     if distribution_name == 'All':
#         base_fontsizes = {'XS': 20, 'S': 35, 'M': 35, 'L': 45, 'XL': 60, 'XXL': 80}

base_row_label_width = 0.35
base_title_spaces = 7
base_sup_title_spaces = 0
if distribution_name == "All":
    base_title_spaces = 8
    base_sup_title_spaces = 25
    base_row_label_width = 0.47

if show_labels:
    base_row_label_width = 0.1
    base_title_spaces = 0
    base_sup_title_spaces = 0
    if distribution_name == "All":
        base_title_spaces = 6
        base_sup_title_spaces = 25
        base_row_label_width = 0.3
        
        
width_ratios = [1.20] + [1] * (len(distributions) - 1)
if distribution_name == "All":
    width_ratios = [1.25] + [1] * (len(distributions) - 1)   
if show_labels:        
    width_ratios = [1.15] + [1] * (len(distributions) - 1)
    if distribution_name == "All":
        width_ratios = [1.18] + [1] * (len(distributions) - 1)

nodes = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.8, 0.9, 1.0]
colors = ['#ffffd9','#edf8b1','#c7e9b4','#7fcdbb','#41b6c4','#1d91c0','#225ea8','#253494','#081d58', '#002652'] #Yellow green blue
# colors = ['#fff7f3','#fde0dd','#fcc5c0','#fa9fb5','#f768a1','#dd3497','#ae017e','#7a0177','#49006a', '#0e0014'] #Red purple
base_cmap = mpl.colors.LinearSegmentedColormap.from_list("mycmap", list(zip(nodes, colors)))
colors.reverse() #flip the colors
reverse_cmap = mpl.colors.LinearSegmentedColormap.from_list("mycmap", list(zip(nodes, colors)))
colors.reverse() #flip back to the original in case we need them again

border_width = 1

#These are the keys that require plots printed in exponential notation and a wider figure size
#figsize = (fig_width * len(distributions), sum(subfig_heights)) #width x height
base_fig_width = 5.2
base_cbar_height = 0.06

if show_labels:
    base_fig_width = 6
    base_cbar_height =0.06

    wider_figure_keys = ['entropy', 'entropy_major']
    wider_fig_width = 7.5
    wider_cbar_height =0.07

    widest_figure_keys = ['avg_opinion_diff', 'avg_local_receptiveness', 'op_var']
    widest_fig_width = 9
    widest_cbar_height = 0.085

#Names for plots and save files
plot_name = {
                # 'log(T)': 'T', 
                'log(T)': 'Tf', 
                'log(T_changed)': 'T_changed',
                'n_clusters': 'clusters', 'entropy' : 'entropy',
                'n_minor' : 'minor_clusters',
                'n_clusters_major' : 'major_clusters',
                'entropy_major' : 'entropy_major',
                'avg_opinion_diff': 'op_diff', 
                'avg_local_agreement' : 'agreement', 
                'avg_local_receptiveness': 'receptiveness',
                'op_mean': 'op_mean', 'op_var': 'op_var', 
                'op_skew': 'op_skew', 'op_kurtosis': 'op_kurtosis'
            }
plot_title = {
                # 'log(T)': r"$\log_{10}(T)$",
                'log(T)': r"$\log_{10}(T_f)$",
                'log(T_changed)': r"$\log_{10}(T_\mathrm{changed})$",
                'n_clusters': 'Number of clusters', 
                'entropy': 'Shannon entropy',
                'n_minor' : 'Number of minor clusters',
                'n_clusters_major' : 'Number of major clusters',
                'entropy_major' : 'Shannon entropy of major clusters',
                'avg_opinion_diff': 'Mean of opinion difference',
                'avg_local_agreement': 'Mean of local agreement', 
                'avg_local_receptiveness': 'Mean of local receptiveness', 
                'op_mean': 'Opinion mean', 'op_var': 'Opinion variance', 
                'op_skew': 'Opinion skew', 'op_kurtosis': 'Opinion kurtosis'
             }


#Statistics to look at
stats = ['avg', 'std']
save_suffix = {'avg':'_avg', 'std':'_std'}
title_prefix = {'avg':'Mean of ', 'std': 'STD of '}
title_prefix = {'avg':'', 'std': 'STD of '}

#Define dataframe columns from consolidated matfiles of random graphs
columns = ['graph', 'd', 'mu', 'weight_set', 'opinion_set',
           'n_clusters', 'n_minor', 'n_clusters_major', 
           'bailout', 'entropy', 'entropy_major', 
           'avg_local_agreement', 'avg_local_receptiveness',
           'T', 'T_changed', 'avg_opinion_diff',
           'op_mean', 'op_var', 'op_skew', 'op_kurtosis']



# Plot heat maps of all simulation values ----------------------------------------------------------

#Loop through the quantities of interest
for key in keys:
    print(key)
    
    #Pick the figure size parameters based on the key
    fig_width = base_fig_width
    fontsizes = base_fontsizes.copy()
    cbar_height = base_cbar_height
    row_label_width = base_row_label_width
    title_spaces = base_title_spaces
    sup_title_spaces = base_sup_title_spaces
    
    if show_labels and print_std:
        if key in wider_figure_keys:
            fig_width = wider_fig_width
            cbar_height = wider_cbar_height
            for size_key in fontsizes.keys():
                fontsizes[size_key] = fontsizes[size_key] * 1.05
        elif key in widest_figure_keys:
            fig_width = widest_fig_width
            cbar_height = widest_cbar_height
            sup_title_spaces = math.floor(base_sup_title_spaces * widest_fig_width / base_fig_width)
            for size_key in fontsizes.keys():
                fontsizes[size_key] = fontsizes[size_key] * 1.05
                
    figsize = (fig_width * len(distributions), sum(subfig_heights)) #width x height
    
    #Pick the colormap based on if we want higher or lower numbers more important
    if key in ['avg_local_receptiveness']: #lower numbers are more important
        cmap = reverse_cmap
    else:
        cmap = base_cmap
    
    heatmaps = {} #dictionary to store the heatmaps for each distribution
    
    for distribution in distributions:
    
        #For random graph models, read and combine the files for each random graph
        if folder_name in ['Erdos-Renyi', 'SBM']:
            
            experiment = folder_name + '/' + graph_type + str(n) + "/" + distribution + '/combined_matfiles'
            if folder_name == 'Erdos-Renyi':
                experiment = experiment + '/p' + str(p)
        
            #Initialize the dataframe
            sim_results = pd.DataFrame(columns = columns)

            #Fill in the data frame row by row and store for each graph number
            for graph_number in graphs:

                filename = experiment + '/graph' + str(graph_number) + '_simulation_results.csv'
                df = pd.read_csv(filename)

                sim_results = pd.concat([sim_results, df], ignore_index=True)
                del df
                
        #For non random graph models or datasets, we only have one graph
        else:
            
            experiment = folder_name + '/' + distribution
            if folder_name == "Complete":
                experiment = folder_name + '/' + graph_type + str(n) + "/" + distribution 

            #Initialize the dataframe
            filename = experiment + '/combined_matfiles/simulation_results.csv'
            sim_results = pd.read_csv(filename)

        #Make sure the graph, weight_set and opinion_set, and time steps are integers instead of floats
        #To make sure we get an average/std with pandas, set the cluster numbers to floats instead of ints
        sim_results = sim_results.astype({'weight_set': int, 'opinion_set': int, 'T': int, 'T_changed': int,
                                         'n_clusters':float, 'n_minor':float, 'n_clusters_major':float})

        #Calculate the log of time steps to make it easier to visualize
        sim_results['log(T)'] = np.log10(sim_results['T'])
        sim_results['log(T_changed)'] = np.log10(sim_results['T_changed'])
    
        #For each segment of the heatmap (a sublist ds in all_ds)
        for section in range(len(all_ds)):
            
            ds = all_ds[section] #Get the confidence bound d values for this section
    
            #Create zero matricies for each heatmap we want to generate
            average, std = np.zeros((len(ds), len(mus))), np.zeros((len(ds), len(mus)))

            #Fill in the heatmap values 
            # rows correspond to d
            for row in range(0,len(ds)):
                d = ds[row]

                # columns correspond to mu
                for col in range(0, len(mus)):
                    mu = mus[col]

                    #filter the data table by the d and mu values
                    df = sim_results[sim_results['d'] == d]
                    df = df[df['mu'] == mu]

                    #Calculate the descriptive stats
                    stats = df[key].describe()

                    #Add the stats values to the appropriate matrix (heatmap)
                    average[row][col] = stats['mean']
                    std[row][col] = stats['std']

            #store the heatmaps
            heatmaps[distribution + '-' + str(section)] = {'avg':average, 'std':std}
    
    #Plot the heatmaps
    for stat in ['avg']: #in heatmaps[distribution].keys():
        
        plot_maps = {heatmap_key: heatmaps[heatmap_key][stat] for heatmap_key in heatmaps.keys()} #collect all the heatmaps of the same quantity - i.e. average, std
        
        fig = plt.figure( constrained_layout=True, facecolor='white', figsize=figsize )
        subfigs = fig.subfigures(1, len(distributions), width_ratios = width_ratios)
        
        #Add a giant combined colorbar at the bottom
        #cbar_ax = fig.add_axes([1.01, .1, .02, .8]) #add colorbar - left, bottom, width, height
        master_subfig_width = 1 - row_label_width / (sum(width_ratios) + row_label_width)
        width = 0.85*master_subfig_width
        left = (1-master_subfig_width) + 0.075*master_subfig_width
        cbar_ax = fig.add_axes([left, -0.15, width, cbar_height])
        
        # cbar_ax = fig.add_axes([.1, -0.08, .85, cbar_height]) 
    
        #Get the maximum value for the colorbar
        vmax = [np.nanmax(plot_maps[heatmap_key]) for heatmap_key in plot_maps.keys()]
        vmax = DW.round_decimals_up(max(vmax), 1)
    
        #Get the minimum value for the color bar
        vmins = []
        for heatmap_key in plot_maps.keys():
            matrix = plot_maps[heatmap_key]
            # If we have -inf values, i.e. if a value is ln(0), then set to 0 so it will plot
            if (np.amin(matrix) == - np.inf):
                matrix[matrix == - np.inf] = 0
                plot_maps[heatmap_key] = matrix
                vmins.append(0)
            else:
                vmins.append(DW.round_decimals_down(np.nanmin(matrix), 1))
        vmin = min(vmins)
    
        #PLot the heatmap for each distribution
        for j in range(len(distributions)):
            distribution = distributions[j]
            subfig = subfigs[j] #single out the right axis
            
            #Section off the subfigure for this distribution into different axs for each heatmap section
            axs = subfig.subplots(len(all_ds), 1, gridspec_kw={'height_ratios': subfig_heights})
            
            for k in range(len(all_ds)):
                ax = axs[k] #single out the right nested subfigure
                
                #Set title for the heatmap for this distribution if we're in the top section
                if k == 0:
                    if distribution in ['Uniform-80-43', 'Uniform-80-20', 'Uniform-90-10']:
                        ax.set_title('Unif' + distribution[7:], fontsize = fontsizes['L'])
                    else:
                        ax.set_title(distribution, fontsize = fontsizes['L'])
                
                heatmap_key = distribution + '-' + str(k)
                matrix = plot_maps[heatmap_key] #get the matrix for the heatmap
                ds = all_ds[k] #get the list of ds for this section

                #Convert matrix to dataframe for axis labels
                df = pd.DataFrame(matrix, index = ds, columns = mus)

                #Plot heat map with or without entry values depending on show_values
                if show_labels:
                    #Get the annotated values of average +/- std
                    if print_std and stat == 'avg':
                        
                        #Get properly formatted precision on labels of avg +/- std
                        avg = np.array(df)
                        std = heatmaps[heatmap_key]['std']
                        labels = DW.print_avg_with_std(avg, std)

                        #Plot heat map with labels of average +/- std
                        g = sns.heatmap(df, ax = ax, 
                                    cbar = (j == 0), cbar_ax = cbar_ax, cmap = cmap, cbar_kws={"orientation": "horizontal"},
                                    vmin = vmin, vmax = vmax, annot = labels, fmt = "", annot_kws={"size":fontsizes['XS']})

                    else:
                        
                        labels = DW.print_sigfigs_nice(matrix, 3)
                        
                        g = sns.heatmap(df, ax = ax, 
                                    cbar = (j == 0), cbar_ax = cbar_ax, cmap = cmap, cbar_kws={"orientation": "horizontal"},
                                    vmin = vmin, vmax = vmax, annot = labels, fmt="", annot_kws={"size":fontsizes['XS']})
                        
                        # g = sns.heatmap(df, ax = ax, 
                        #             cbar = (j == 0), cbar_ax = cbar_ax, cmap = cmap, cbar_kws={"orientation": "horizontal"},
                        #             vmin = vmin, vmax = vmax, annot = True,
                        #             fmt="0.2e", annot_kws={"size":fontsizes['XS']})
                else:
                    g = sns.heatmap(df, ax = ax, 
                                    cbar = (j == 0), cbar_ax = cbar_ax, cmap = cmap, cbar_kws={"orientation": "horizontal"},
                                    vmin = vmin, vmax = vmax, annot = False)
                #ax = sns.heatmap(df, cmap = "rainbow", vmin = vmin, vmax = vmax)
                
                #Set border for heatmaps
                for spine in ax.spines.values():
                    spine.set(visible=True, lw=border_width, edgecolor="black")
                
                #Set y ticks only if we are the leftmost distributions
                if j == 0:
                    ax.set_yticklabels(ds, rotation = 0, size = fontsizes['S'])
                else:
                    ax.axes.yaxis.set_visible(False)
                    
                #Set the x ticks if we are in the bottom section for each heatmap
                if k == len(all_ds) - 1:
                    ax.set_xticklabels(mus, size = fontsizes['S'])
                #Otherwise bootleg show the labels in white to create some space between heatmap sections
                else:
                    ax.set_xticklabels(mus, size = fontsizes['XXS'])
                    ax.tick_params(axis='x', colors='white')
                    
                # plt.subplots_adjust(hspace=0.1)
                
        #Set colorbar and tick label font size
        cbar_ax.tick_params(labelsize=fontsizes['M'])
        cbar_ax.set_xlabel(title_prefix[stat] + plot_title[key], fontsize=fontsizes['XL'], labelpad=fontsizes['S'])
        cbar_ax.tick_params(axis='both', labelsize=fontsizes['M'])
        cbar_ax.spines["outline"].set(visible=True, lw=border_width, edgecolor="black")
        cbar_ax.xaxis.tick_top()
        
        #Large shared axis labels
        fig.supylabel(" "*4 + r"Confidence bound ($c$)", fontsize = fontsizes['XL'])
        fig.supxlabel(" "*title_spaces + r"Compromise parameter ($m$)", fontsize = fontsizes['XL'])
    
        #Large supertitle
        if show_suptitle:
            suptitle = "" + " "*sup_title_spaces
            if graph_type == "complete":
                suptitle = title_prefix[stat] + plot_title[key] + " for C(" + str(n) + ")"
            elif graph_type == "erdos-renyi":
                suptitle = title_prefix[stat] + plot_title[key] + " for G(n=" + str(n) + ", p=" + str(p) + ")"
            elif graph_type == "SBM-2-community":
                suptitle = title_prefix[stat] + plot_title[key] + " for 2-Community SBM"
            elif graph_type == "SBM-core-periphery":
                suptitle = title_prefix[stat] + plot_title[key] + " for Core--Periphery SBM"
            elif graph_type == "Caltech":
                suptitle = title_prefix[stat] + plot_title[key] + " for Caltech"
            plt.suptitle(suptitle, fontsize = fontsizes['XXL'])
        
        #Save the plot
        savefile = directory + "/"
        if graph_type == 'erdos-renyi':
            savefile = f"{directory}/ER{n}-p{p}"
        elif graph_type == "Caltech":
            savefile = f"{directory}/Caltech"
        else:
            savefile = f"{directory}/{graph_type}{n}"
        if distribution_name != "All":
            savefile = f"{savefile}--{distribution_name}"
        savefile = f"{savefile}--{plot_name[key]}{save_suffix[stat]}"
        # savefile = directory + '/' + ("p" + str(p) + "--") * (graph_type == "erdos-renyi")  
        # savefile = savefile + distribution_name + '--' + plot_name[key] + save_suffix[stat]
        savefile = savefile + ('--labeled' * show_labels) + ('--with_std' * print_std * (stat == 'avg')) + ".png"
        plt.savefig(savefile, bbox_inches='tight', facecolor='white')
        # plt.show()
        
        plt.close()