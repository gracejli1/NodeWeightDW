# NodeWeightDW

This repository was developed by Grace Li. It contains the source code and plots for [our paper](https://journals.aps.org/prresearch/abstract/10.1103/PhysRevResearch.5.023179):

G. Li and M. A. Porter. “Bounded-Confidence Model of Opinion Dynamics with Heterogeneous Node-Activity Levels.” *Physical Review Research*, 5(2): 023149. 2023.

This paper is also on arXiv at [https://arxiv.org/abs/2206.09490](https://arxiv.org/abs/2206.09490).

We generalize the Deffuant–Weisbuch (DW) bounded-confidence model (BCM) of opinion dynamics by incorporating node weights that allow us to model agents with different probabilities of interacting.

## An Important Note on Distribution Names

Initially we had called the distributions that had the same mean as a Pareto distribution with parameter $\alpha$ = log(10) / log(4.5) "80-10" distributions. However, as we detail in our erratum such distributions are actually "80-43". In the code and plots in this repository, we have changed the distribution names to "Pareto-80-43", "Exp-80-43", and "Uniform-80-43" so that the distribution name and parameter now correctly match.

## Plots

To navigate to the plots we reference in the paper, open the `FinalPlots` folder and go to the subfolder of the corresponding network type.

## Code

### Requirements

The code was developed using Python 3.8.10. Package versions used for development are listed below.

```
python-igraph     0.9.7
numpy             1.21.3 
pandas            1.3.4
scipy             1.8.0 
matplotlib        3.4.3 
seaborn           0.12.0 
```

### Navigating the Code

I implemented our DW model with node weights in `DW.py`.

More information on the distributions of node weights that we examined can be found in `DistributionInfo.ipynb`.

Our simulation pipeline is as follows:
1. Our simulations are run using `syntheticGraphSimulations.py` for synthetic graphs and `Caltech/CaltechSimulations.py` for the Caltech network data set.
2. After running our simulations, `MatfileConsolidatorHK.py` is used to parse through the outputs and generate `.csv` files tabulating the results for each node-weight distribution. These files can be found in the `combined_results` folder of appropriate distribution folder in the appropriate network folder. For example, `Complete/complete500/Pareto-80-20/combined_matfiles`.
3. Using the tabulated outputs of `MatfileConsolidatorHK.py`, the `Heatmaps.py` script generates the heatmaps we present in our paper for SBM random graphs and the Caltech network. For complete graphs, we examined more node-weight distributions and visualized them with the `Complete/Heatmaps-VerticalCbar.py` script. For Erdos-Renyi random graphs, we stacked the heatmaps for various edge-probability parameters $p$ of the $G(N, p)$ random-graph model using the `Erdos-Renyi/Heatmaps-stacked-p.py` script. 
4. As an alternative to heatmap visualizations, the `LineplotsOverlay.py` and `LineplotsGrid.py` scripts generate line plots instead of heatmaps to visualize the results of our simulations.
5. The `OpinionTrajectories.py` script visualizes the opinion trajectories of nodes in our simulations. It generates plots of opinion versus time and each node in the network is represented by a curve on the plot.

## Citation

If you find this respository useful in your research, please consider citing the our paper:

G. Li and M. A. Porter. “Bounded-Confidence Model of Opinion Dynamics with Heterogeneous Node-Activity Levels.” *Physical Review Research*, 5(2): 023149. 2023.