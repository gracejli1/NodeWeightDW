"""
Created on 11/19/2022
Created by Grace Li

This script generates a figure consiting of a grid of line plots of the results of the simulations for our DW model with node weights.

In the paper, we examine the numbers of major and minor clusters, Shannon entropy, mean local receptiveness and convergence time.
Here, we also plot the variance of final opinions and the convergence time in terms of time-steps where opinions changed.

For each quantity of interest, this script generates a figure consisting of a grid of line plots. Each line plot represents a single 
distribution of node weights, and has the quantity of interest on the vertical axis and the confidence bound c (named d in the code)
on the horizontal axis. Each line plot has curves (indicated by color and marker) to represent each value of the compromise parameter m
(named mu in the code). There is an option to show or hide an error band representing one standard deviation.

For fixed graphs (i.e., complete graphs and the Caltech network), we generate each point on each line plot from 100 numerical
simulations from 10 sets of node weights that each have 10 sets of initial opinions. For random graphs (i.e., Erdos-Renyi and SBM graphs), 
we generate each point on each line plot from 500 numerical simulations from 5 random graphs each with 10 sets of node weights 
that each have 10 sets of initial opinions.

"""

"""
Note 5/2/2024: Initially we had called the distributions that had the same mean as a Pareto distribution with parameter alpha = log(0.1) / log(0.2/0.9) "80-10" distributions. However, as we detail in our erratum such distributions are actually "80-43". In this script, we have changed the names to "Pareto-80-43", "Exp-80-43", and "Uniform-80-43" so that the distribution name now correct match.
"""

# Import required packages
import numpy as np
import pandas as pd
from scipy import io
import sys
import random
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import animation
import seaborn as sns
import time as time
import igraph as igraph
import os.path
from os import getpid
import multiprocessing

import DW as DW #import our own DW module

sns.set_style("ticks",{'axes.grid' : True})

## Change parameters here ------------------------------------------------

#Options for graph_type are complete, erdos-renyi, SBM-2-community, SBM-core-periphery, Caltech
graph_type = "complete"
n = 500 #number of nodes in synthetic graph
p = 0.1 #independent edge probability for erdos-renyi graphs

show_std = True

show_suptitle = False #whether to print a giant suptitle for the plot

#Specify which quantities to generate plots of
keys = [
        'log(T)', 'log(T_changed)', 
        'entropy',
        'n_minor', 'n_clusters_major',
        'avg_local_receptiveness',
        'op_var'
       ]
# keys = ['log(T)']

#specify the numbers of the random graphs for erdos-renyi and SBM
graphs = list(range(5))

#Compromise parameter
'''
NOTE: THE COMPROMISE PARAMETER IS CALLED mu HERE, BUT m IN THE PAPER
'''
mus = [0.1, 0.3, 0.5]

#Define the confidence bound sections for each distribution
'''
NOTE: THE CONFIDENCE BOUND IS CALLED d HERE, BUT c IN THE PAPER
'''     
drop_ds = [0.7, 0.9]
min_d = 0.1
max_d = 0.5

custom_xticks = True
xtick_list = [0.1, 0.2, 0.3, 0.4, 0.5]

#Distributions
distributions = ['Constant', 'Uniform-80-43', 'Uniform-80-20', 
                 'Exp-80-43', 'Exp-80-20', 'Pareto-80-43', 'Pareto-80-20']
biggrid = False
rows, cols = 2, 4
bbox_to_anchor = (0.88, 0.79)

if graph_type == "complete" and n == 500:
    distributions = ['Uniform-80-43', 'Uniform-80-20', 'Uniform-90-10', 'Constant', 
                    'Exp-80-43', 'Exp-80-20', 'Exp-90-10',
                    'Pareto-80-43', 'Pareto-80-20', 'Pareto-90-10']
    biggrid = True
    rows, cols = 3, 4
    bbox_to_anchor = (0.19, 0.84)

#Name of experiment folder
folder_name_dict = {"complete": "Complete", "erdos-renyi": "Erdos-Renyi",
                    "SBM-2-community": "SBM", "SBM-core-periphery": "SBM",
                    "Caltech": "Caltech"}
folder_name = folder_name_dict[graph_type]

#Check that a directory exists, and if not, create it
directory = folder_name 
if folder_name != 'Caltech':
    directory = directory + '/' + graph_type + str(n)
directory = directory + '/combined_plots/lineplots' + ('/title' * show_suptitle)
if graph_type == "erdos-renyi":
    directory = directory + f"/p{p}"
if not os.path.exists(directory):
    os.makedirs(directory)

## Plot parameters -----------------------------------------------------------

fontsizes = {'left_space': 40, 'bottom_space':20, 'XS': 10, 'S': 25, 'M': 30, 'L':35, 'XL': 55, 'XXL':50}
sup_title_spaces = 3

plot_height = 5.8
if show_suptitle:
    plot_height = 6.1
plot_width = 5.8

#Color, marker and dash style for each of the three mu values in the plot
#https://davidmathlogic.com/colorblind/ -> tol
# colorlist = ['#332288', '#117733', '#AA4499']
colorlist = ['#0173b2', '#de8f05', '#029e73']
markerlist = ["o", "X", "^"]
dashlist = ['', (3,3), (1, 1)]

#Names for plots and save files
plot_name = {
                'log(T)': 'T', 'log(T_changed)': 'T_changed',
                'n_clusters': 'clusters', 'entropy' : 'entropy',
                'n_minor' : 'minor_clusters',
                'n_clusters_major' : 'major_clusters',
                'entropy_major' : 'entropy_major',
                'avg_opinion_diff': 'op_diff', 
                'avg_local_agreement' : 'agreement', 
                'avg_local_receptiveness': 'receptiveness',
                'op_mean': 'op_mean', 'op_var': 'op_var', 
                'op_skew': 'op_skew', 'op_kurtosis': 'op_kurtosis'
            }
plot_title = {
                'log(T)': r"$\log_{10}(T)$", 
                'log(T_changed)': r"$\log_{10}(T_{\mathrm{changed}})$",
                'n_clusters': 'Number of clusters', 
                'entropy': 'Shannon entropy',
                'n_minor' : 'Number of minor clusters',
                'n_clusters_major' : 'Number of major clusters',
                'entropy_major' : 'Shannon entropy of major clusters',
                'avg_opinion_diff': 'Mean of opinion difference',
                'avg_local_agreement': 'Mean of local agreement', 
                'avg_local_receptiveness': 'Mean of local receptiveness', 
                'op_mean': 'Opinion mean', 'op_var': 'Opinion variance', 
                'op_skew': 'Opinion skew', 'op_kurtosis': 'Opinion kurtosis'
             }



#Define dataframe columns from consolidated matfiles of random graphs
columns = ['distribution', 'graph', 'd', 'mu', 'weight_set', 'opinion_set',
           'n_clusters', 'n_minor', 'n_clusters_major', 
           'bailout', 'entropy', 'entropy_major', 
           'avg_local_agreement', 'avg_local_receptiveness',
           'T', 'T_changed', 'avg_opinion_diff',
           'op_mean', 'op_var', 'op_skew', 'op_kurtosis']



# Plot heat maps of all simulation values ----------------------------------------------------------

#Loop through the quantities of interest
for key in keys:
    print(key)
    
    fig, axs = plt.subplots(rows, cols, sharey=True, #constrained_layout=True, 
                            figsize = (plot_width*cols, plot_height*rows) )
    
    distribution_idx = -1
    
    for row in range(rows):
        for col in range(cols):
            
            ax = axs[row, col]
            
            #Leave the top right corner blank to put the legend if not making a big grid plot
            if (not biggrid) and row == 0 and col == cols-1:
                ax.set_axis_off()
            
            #If making a big-grid plot, the constant distribution is the only plot in the left-most column
            elif biggrid and (row in [0,2] and col == 0):
                ax.set_axis_off()
                
            #PLot the line plot for each distribution
            else:
                distribution_idx += 1
                distribution = distributions[distribution_idx]
    
                #Initialize the dataframe
                sim_results = pd.DataFrame(columns = columns)

                #For random graph models, read and combine the files for each random graph
                if folder_name in ['Erdos-Renyi', 'SBM']:

                    experiment = folder_name + '/' + graph_type + str(n) + "/" + distribution + '/combined_matfiles'
                    if folder_name == 'Erdos-Renyi':
                        experiment = experiment + '/p' + str(p)

                    #Fill in the data frame row by row and store for each graph number
                    for graph_number in graphs:

                        filename = experiment + '/graph' + str(graph_number) + '_simulation_results.csv'
                        df = pd.read_csv(filename)

                        sim_results = pd.concat([sim_results, df], ignore_index=True)
                        del df

                #For non random graph models or datasets, we only have one graph
                else:

                    experiment = folder_name + '/' + distribution
                    if folder_name == "Complete":
                        experiment = folder_name + '/' + graph_type + str(n) + "/" + distribution 

                    #Initialize the dataframe
                    filename = experiment + '/combined_matfiles/simulation_results.csv'
                    sim_results = pd.read_csv(filename)

                distribution_name = distribution
                if distribution in ['Uniform-80-43', 'Uniform-80-20', 'Uniform-90-10']:
                    distribution_name = 'Unif' + distribution[7:]

                #Make sure the graph, weight_set and opinion_set, and time steps are integers instead of floats
                #To make sure we get an average/std with pandas, set the cluster numbers to floats instead of ints
                sim_results = sim_results.astype({'weight_set': int, 'opinion_set': int, 'T': int, 'T_changed': int,
                                                 'n_clusters':float, 'n_minor':float, 'n_clusters_major':float})

                #Calculate the log of time steps to make it easier to visualize
                sim_results['log(T)'] = np.log10(sim_results['T'])
                sim_results['log(T_changed)'] = np.log10(sim_results['T_changed'])

                # #Create a new column for full name mu for the legend
                # sim_results['legend_label'] = r"$\mu=$" + sim_results['mu'].astype(str)

                #Drop the rows of df with c values we don't want to include in the plot
                sim_results = sim_results[~sim_results['d'].isin(drop_ds)]

                #Generate line plot for this distribution 
                if show_std:
                    sns.lineplot(x ='d', y = key, ax = ax, data = sim_results, errorbar='sd',
                                 hue = "mu",  palette = colorlist,
                                 style = "mu", markers = markerlist, dashes = dashlist, markersize=10)
                else:
                    sim_results = sim_results.groupby(['d'], as_index=False).mean()
                    sns.lineplot(x ='d', y = key, ax = ax, data = sim_results,
                                 hue = "mu",  palette = colorlist,
                                 style = "mu", markers = markerlist, dashes = dashlist, markersize=10)

                #Hide the default seaborn legend
                ax.get_legend().remove()

                if distribution_idx == 0:
                    legend = fig.legend(title = r"$m$", loc='center', bbox_to_anchor=bbox_to_anchor,
                                        fontsize = fontsizes['L'], title_fontsize = fontsizes['L'], markerscale=2)
        
                ax.set_title(distribution_name, fontsize = fontsizes['L'], pad = fontsizes['XS'])

                if biggrid:
                    if row == 1 and col == 0:
                        ax.set_ylabel(plot_title[key]+"\n", fontsize = fontsizes['XL'], va='center', labelpad = 25)
                    else:
                        ax.set(ylabel=None)  
                    if row == 2 and col == 2:
                        ax.set_xlabel(r"Confidence Bound ($d$)", fontsize = fontsizes['XL'], labelpad = 20) 
                    else:
                        ax.set(xlabel=None)
                    
                else:
                    if col == 0:
                        ax.set_ylabel(" ", fontsize = fontsizes['left_space'])
                        ax.yaxis.label.set_color('white')
                    else:
                        ax.set(ylabel=None)

                    if row == rows - 1:
                        ax.set_xlabel(" ", fontsize = fontsizes['bottom_space'])
                        ax.xaxis.label.set_color('white')
                    else:
                        ax.set(xlabel=None)
                
                ax.tick_params(axis = 'both', labelsize = fontsizes['S'])
                ax.set_xlim(min_d - 0.01, max_d + 0.01)
                if custom_xticks:
                    ax.set_xticks(xtick_list)

    #Make all axes tick makrs visible
    for ax in axs.flatten():
        ax.xaxis.set_tick_params(labelbottom=True)
        ax.yaxis.set_tick_params(labelleft=True)
                    
    # Large axis labels
    if not biggrid:
        fig.supylabel(" "*3 + plot_title[key], fontsize = fontsizes['XL'], va='center')
        fig.supxlabel(r"Confidence Bound ($d$)", fontsize = fontsizes['XL'])
    
    #Large supertitle
    if show_suptitle:
        suptitle = " "*sup_title_spaces + plot_title[key]
        if graph_type == "complete":
            suptitle = suptitle + f" for C({n})"
        elif graph_type == "erdos-renyi":
            suptitle = suptitle + f" for G(n={n}, p={p})"
        elif graph_type == "SBM-2-community":
            suptitle = suptitle + " for 2-community SBM"
        elif graph_type == "SBM-core-periphery":
            suptitle = suptitle + " for core--periphery SBM"
        elif graph_type == "Caltech":
            suptitle = suptitle + " for Caltech network"
        plt.suptitle(suptitle, fontsize = fontsizes['XXL'])

    #Padding between plots
    # fig.tight_layout(pad=1.0)
    fig.tight_layout(w_pad=3, h_pad=3)
    # fig.subplots_adjust(wspace=2)
        
    #Save the plot
    savefile = directory
    if graph_type == "erdos-renyi":
        savefile = savefile + f"/ER{n}-p{p}"
    else:
        savefile = savefile + f"/{graph_type}{n}"
    savefile = savefile + '--Grid--' + plot_name[key] + ".png"
    plt.savefig(savefile, bbox_inches='tight', facecolor='white')
    plt.show()
    plt.close()